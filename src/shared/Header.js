import { HEADER_COLOR } from './Theme';

const HeaderFactory = ({ headerTintColor, backgroundColor }) => ({
  headerTintColor,
  headerBackTitle: null,
  headerStyle: {
    backgroundColor,
    borderBottomWidth: 0,
    elevation: 0,
    height: 65,
  },
});

export const HEADER_BLUE = HeaderFactory({ headerTintColor: '#FFF', backgroundColor: HEADER_COLOR });
export const HEADER_WHITE = HeaderFactory({ headerTintColor: '#000', backgroundColor: '#FFF' });
