export const LIGHT_BLUE = '#00B8FF';
export const DARK_BLUE = '#5A92FF';

export const SECONDARY_COLOR = '#E6F4F1';
export const ACCENT_COLOR = '#00287A';

export const GRAY = '#dcdcdc';

//  Alternative for LIGHT_BLUE due to colors inconcsistency
// Will be used until real theme provider is introduced
export const HEADER_COLOR = '#1fb9fc';
