// AUTH ROUTES

export const LOGIN_ROUTE = 'LOGIN';
export const SIGNUP_ROUTE = 'SIGNUP';

// MAIN STACK

export const MAIN_TAB_ROUTE = 'MAIN_TAB'; // (1)
export const FEED_PROFILE_ROUTE = 'FEED_PROFILE';
export const BADGE_PREVIEW_ROUTE = 'BADGE_PREVIEW';
export const INSTRUCTIONS_ROUTE = 'INSTRUCTIONS';
export const AVAILABLE_PEAKS_ROUTE = 'AVAILABLE_PEAKS';

// (1) MAIN TAB ROUTES

export const HOME_ROUTE = 'HOME';
export const RANK_ROUTE = 'RANK';
export const SCAN_ROUTE = 'SCAN';
export const PROFILE_ROUTE = 'MY_PROFILE';

export const SETTINGS_ROUTE = 'SETTINGS';
export const LANGUAGE_ROUTE = 'LANGUAGE';
export const DELETE_ACCOUNT_ROUTE = 'DELETE_ACCOUNT';
