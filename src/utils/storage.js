import AsyncStorage from '@react-native-community/async-storage';


export const setItem = (key, data) => {
  if (data) {
    AsyncStorage.setItem(key, JSON.stringify(data));
  } else {
    AsyncStorage.removeItem(key);
  }
};

export const getItem = async (key) => {
  const payload = await AsyncStorage.getItem(key);

  if (payload) {
    try {
      return JSON.parse(payload);
    } catch (error) {
      return undefined;
    }
  }
  return undefined;
};
