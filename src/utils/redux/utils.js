export const concatToList = (list, item) => (list).concat(item);
export const removeFromList = (list, item) => (list).filter(_item => _item.id !== item.id);
