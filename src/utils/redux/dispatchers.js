/**
 * Dispatches action to store
 *
 * @name dispatcherCreator
 *
 * @param {Action}   - action for dispatch
 * @param {Dispatch} - dispatch
 */
export const dispatcherCreator = action => dispatch => (payload) => {
  dispatch(action({ payload }));
};


/**
 * Resolves asynchronous workflow and accordingly dispatches actions to store
 *
 * @name asyncDispatcherCreator
 *
 * @param {Actions} actions  - actions for dispatch
 * @param {*}       asyncJob - asynchronous function
 * @param {*}       meta     - parameters assignable to asyncJob function
 */
export const asyncDispatcherCreator = (
  actions,
  asyncJob
) => meta => async (dispatch) => {
  dispatch(actions.started({ meta }));

  try {
    const { payload } = await asyncJob(meta);
    dispatch(actions.success({ payload }));
    return Promise.resolve(payload);
  } catch (error) {
    dispatch(actions.failure({ error }));
    throw error;
  }
};


/**
 * Enables invoking asyncDispatherCreator method with parameters provided in different order.
 * Usefull when maping dispatchers to props.
 * Parameters and generic types are equivalent to those of asyncDispatcherCreator function.
 *
 * @name asyncDispatcherAdapter
 */
export const asyncDispatcherAdapter = (
  actions,
  asyncJob,
) => dispatch => meta => asyncDispatcherCreator(actions, asyncJob)(meta)(dispatch);
