/**
 * Returns action with defined type and optional payload/error
 *
 * @name actionCreator
 *
 * @param {String} type   - action type
 * @param {*}      params - action parameters
 */
export const actionCreator = type => params => ({
  type,
  ...params
});


/**
 * Returns object with defined type and three action creators : started, success & failure
 *
 * @name asyncActionCreator
 *
 * @param {String} type - action type
 */
export const asyncActionCreator = type => ({
  type,
  started: actionCreator(`${type}_STARTED`),
  success: actionCreator(`${type}_SUCCESS`),
  failure: actionCreator(`${type}_FAILURE`),
});
