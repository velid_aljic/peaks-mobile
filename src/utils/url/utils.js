export const getLastSegmentOfUrl = (url = '') => {
  const splitedUrl = url.split('/');
  const notEmptySegments = splitedUrl.filter(segment => !!segment);
  return notEmptySegments[notEmptySegments.length - 1];
};
