import { isEmpty } from 'lodash';

import { buildParamsArray, pathBuilder } from './core';


const DEFAULT_SORT_PARAMS_PATTERN = ({ sortBy, order }) => `${sortBy},${order}`;

const compiler = (path, params) => {
  // eslint-disable-next-line
  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      const val = params[key];
      path = path.replace(`{${key}}`, val.toString()); // eslint-disable-line
    }
  }

  return path;
};

export const pathFrom = pathBuilder(compiler);

export const searchFrom = (
  {
    filtersConfig = {},
    page,
    size,
    sortConfig = [],
    ...rest
  } = {},
  sortPattern = DEFAULT_SORT_PARAMS_PATTERN,
) => {
  const params = buildParamsArray({
    page, size, ...filtersConfig, ...rest
  }).map(paramArray => paramArray.filter(e => !!e).join('='),);

  const sortArray = buildParamsArray({ sort: sortConfig.map(s => sortPattern(s)) }).map(
    paramArray => paramArray.join('='),
  );

  return params.concat(sortArray).join('&');
};

export const compose = (path = '', pathParams = {}, search = {}) => `${pathFrom(path, pathParams)}${isEmpty(search) ? '' : `?${searchFrom(search)}`}`;
