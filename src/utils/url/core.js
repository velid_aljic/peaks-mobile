import { isArray, isNil, isUndefined } from 'lodash';


/* eslint-disable */
export const buildParamsArray = ({ ...params } = {}) => {
  const query = [];

  const prepareKeyValue = (key, value) => [key, isNil(value) ? '' : encodeURIComponent(value)];

  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      const value = params[key];

      if (isArray(value)) {
        for (const partialQuery of value) {
          query.push(prepareKeyValue(key, partialQuery));
        }
      } else if (!isUndefined(value)) {
        query.push(prepareKeyValue(key, value));
      }
    }
  }

  return query;
};

export const pathBuilder = compiler => (
  path = '',
  params = {},
) => compiler(path, params);
