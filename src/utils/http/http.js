import { HOST } from 'react-native-dotenv';
import axios from 'axios';

import { execute } from './core';
import { stringifyData } from './utils';


export const get = (url, options) => execute(url, options);


export const post = (url, data, options) => {
  const request = Object.assign({}, options, { data: stringifyData(data), method: 'POST' });
  return execute(url, request);
};


export const put = (url, data, options) => {
  const request = Object.assign({}, options, { data, method: 'PUT' });
  return execute(url, request);
};


export const patch = (url, data, options) => {
  const request = Object.assign({}, options, { data, method: 'PATCH' });
  return execute(url, request);
};


export const remove = (url, options) => {
  const request = Object.assign({}, options, { method: 'DELETE' });
  return execute(url, request);
};


axios.defaults.baseURL = (() => HOST)();
axios.defaults.timeout = 10000;


export const http = axios;
