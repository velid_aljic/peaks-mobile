import axios from 'axios';
import queryString from 'query-string';

export const stringifyData = data => queryString.stringify(data);
export const getCancelTokenSource = () => axios.CancelToken.source();
