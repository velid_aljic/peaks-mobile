export class HttpError extends Error {
  constructor(error = {}) {
    const {
      canceled = false,
      errors = [],
      message = '',
      network = true,
      payload, status,
      timeout = false,
    } = error;

    super(message);

    this.canceled = canceled;
    this.errors = errors;
    this.message = message;
    this.name = 'HttpError';
    this.network = network;
    this.payload = payload;
    this.status = status;
    this.timeout = timeout;
  }
}
