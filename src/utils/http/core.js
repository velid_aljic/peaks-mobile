import axios from 'axios';

import { HttpError } from './error';


const URL_ENCODED = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
};


axios.interceptors.response.use(
  response => Promise.resolve(onSuccess(response)),
  error => Promise.reject(onFailure(error)),
);


/**
 * @name execute
 *
 * @param {*} url
 * @param {*} options
 */
export const execute = async (url, options = URL_ENCODED) => {
  const response = await axios(url, options);
  return response.data;
};


/**
 * @name onSuccess
 *
 * @param {*} response
 */
const onSuccess = (response) => {
  if (!response.data) {
    response.data = {};
  }
  return response;
};


/**
 * @name onFailure
 *
 * @param {AxiosError} axiosError                             -
 * @param {Object}     axiosError.response                    -
 * @param {Object}     axiosError.response.data               - Server response
 * @param {Object}     axiosError.response.data.error         -
 * @param {String}     axiosError.response.data.error.code    -
 * @param {String}     axiosError.response.data.error.message -
 * @param {Object}     axiosError.response.data.error.errors  -
 * @param {Object}     axiosError.response.status             -
 * @param {Object}     axiosError.response.config             -
 */
const onFailure = (axiosError) => {
  if (axiosError.response) {
    const { data, status } = axiosError.response;
    const { message, errors } = data.error;

    return new HttpError({
      errors,
      message,
      payload: data,
      status,
    });
  }

  const { message } = axiosError;

  // Canceled by user action
  if (axios.isCancel(axiosError)) {
    return new HttpError({ message, canceled: true, network: true });
  }

  // Timeout exceeded
  if (axiosError.code === 'ECONNABORTED') {
    return new HttpError({ message, timeout: true, network: true });
  }

  // Network error
  return new HttpError({ message, network: false });
};
