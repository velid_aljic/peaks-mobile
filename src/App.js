import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { Root } from 'native-base';
import React from 'react';

import {
  Info as AppInfo,
  Bus,
  ErrorsBoundry,
  Loader,
} from '@components/Application';
import { Update as AppUpdate } from '@components/Update';
import { Language } from '@components/Language';
import { Notifications } from '@components/Notifications';

import { onSuccessRequest } from '@data/shared/interceptors';
import { persistor, store } from '@data/store';

import { http } from '@utils/http';

import { EntryPage } from '@screens/Entry';


//* INTERCEPTORS

http.interceptors.request.use((request) => {
  onSuccessRequest(request);
  return request;
});

http.interceptors.response.use(
  response => response,
  (error) => {
    Bus.publishHttpError(error);
    return Promise.reject(error.message || error);
  }
);

//* EXPORT

export class App extends React.Component {
  componentDidMount() {
    Notifications.configure();
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<Loader />} persistor={persistor}>
          <Language>
            <Root>
              <ErrorsBoundry>
                <AppInfo />
                <AppUpdate />
                <EntryPage />
              </ErrorsBoundry>
            </Root>
          </Language>
        </PersistGate>
      </Provider>
    );
  }
}
