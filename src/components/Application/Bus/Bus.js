import { isNumber } from 'lodash';
import PubSub from 'pubsub-js';

import {
  HTTP_400,
  HTTP_401,
  HTTP_403,
  HTTP_404,
  HTTP_409,
  HTTP_5XX,
  HTTP_NETWORK,
  HTTP_TIMEOUT
} from './topics';


class BusClass {
  pubSub = PubSub;

  publish(topic, data) {
    return this.pubSub.publish(topic, data);
  }

  subscribe(topic, listener) {
    return this.pubSub.subscribe(topic, listener);
  }

  unsubscribe(token) {
    return this.pubSub.unsubscribe(token);
  }

  publishHttpError(error) {
    if (error.status === 400) {
      return this.pubSub.publish(HTTP_400, error);
    }

    if (error.status === 401) {
      return this.pubSub.publish(HTTP_401, error);
    }

    if (error.status === 403) {
      return this.pubSub.publish(HTTP_403, error);
    }

    if (error.status === 404) {
      return this.pubSub.publish(HTTP_404, error);
    }

    if (error.status === 409) {
      return this.pubSub.publish(HTTP_409, error);
    }

    if (isNumber(error.status) && error.status >= 500) {
      return this.pubSub.publish(HTTP_5XX, error);
    }

    if (!error.network) {
      return this.pubSub.publish(HTTP_NETWORK, error);
    }

    if (error.timeout) {
      return this.pubSub.publish(HTTP_TIMEOUT, error);
    }

    return null;
  }
}

export const Bus = new BusClass();
