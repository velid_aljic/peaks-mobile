// APP INFO

export const APP_INFO = 'APP_INFO';

// APP UNCAUGHT ERROR

export const APP_UNCAUGHT = 'APP_UNCAUGHT';

// HTTP ERROR TOPICS

export const HTTP_400 = 'HTTP_400';
export const HTTP_401 = 'HTTP_401';
export const HTTP_403 = 'HTTP_403';
export const HTTP_404 = 'HTTP_404';
export const HTTP_409 = 'HTTP_409';
export const HTTP_5XX = 'HTTP_5XX';
export const HTTP_NETWORK = 'HTTP_NETWORK';
export const HTTP_OFFLINE = 'HTTP_OFFLINE';
export const HTTP_TIMEOUT = 'HTTP_TIMEOUT';
