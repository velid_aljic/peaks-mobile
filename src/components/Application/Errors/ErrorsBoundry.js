import React from 'react';
import firebase from 'react-native-firebase';

import {
  APP_UNCAUGHT,

  Bus,

  HTTP_400,
  HTTP_401,
  HTTP_403,
  HTTP_404,
  HTTP_409,
  HTTP_5XX,
  HTTP_NETWORK,
  HTTP_OFFLINE,
  HTTP_TIMEOUT
} from '@components/Application/Bus';


/**
 * @name ErrorsBoundryComponent
 *
 * @prop  {Boolean}   authenticated    - Is user authenticated
 * @prop  {String}    email            - User's email
 * @prop  {String}    refreshToken     - User's refresh token
 *
 * @prop  {Function}  refreshAuthToken - Method to call to refresh auth token
 * @arg   {String}    email            - User's email
 * @arg   {String}    refreshToken     - User's refresh token
 */
export class ErrorsBoundryComponent extends React.Component {
  componentWillMount() {
    this.appUncaught = Bus.subscribe(APP_UNCAUGHT, this.handleAppUncaught);

    this.badRequest = Bus.subscribe(HTTP_400, this.handleBadRequest);
    this.unauthorized = Bus.subscribe(HTTP_401, this.handleUnauthorized);
    this.forbidden = Bus.subscribe(HTTP_403, this.handleForbidden);
    this.notFound = Bus.subscribe(HTTP_404, this.handleNotFound);
    this.conflict = Bus.subscribe(HTTP_409, this.handleConflict);
    this.server = Bus.subscribe(HTTP_5XX, this.handleServer);
    this.network = Bus.subscribe(HTTP_NETWORK, this.handleNetwork);
    this.offline = Bus.subscribe(HTTP_OFFLINE, this.handleOffline);
    this.timeout = Bus.subscribe(HTTP_TIMEOUT, this.handleTimeout);
  }

  componentWillUnmount() {
    Bus.unsubscribe(this.appUncaught);
    Bus.unsubscribe(this.badRequest);
    Bus.unsubscribe(this.unauthorized);
    Bus.unsubscribe(this.forbidden);
    Bus.unsubscribe(this.notFound);
    Bus.unsubscribe(this.conflict);
    Bus.unsubscribe(this.server);
    Bus.unsubscribe(this.network);
    Bus.unsubscribe(this.offline);
    Bus.unsubscribe(this.timeout);
  }


  //* COMPONENT METHODS

  // eslint-disable-next-line
  handleAppUncaught = (topic, error) => {
    firebase.analytics().logEvent(APP_UNCAUGHT);
  };

  // eslint-disable-next-line
  handleBadRequest = (topic, error) => {
    firebase.analytics().logEvent(HTTP_400);
  }

  // eslint-disable-next-line
  handleUnauthorized = (topic, error) => {
    firebase.analytics().logEvent(HTTP_401);

    const { email, refreshToken } = this.props;
    if (refreshToken !== null) {
      this.props.refreshAuthToken({ email, refreshToken });
    }
  };

  // eslint-disable-next-line
  handleForbidden = (topic, error) => {
    firebase.analytics().logEvent(HTTP_403);
  };

  // eslint-disable-next-line
  handleNotFound = (topic, error) => {
    firebase.analytics().logEvent(HTTP_404);
  };

  // eslint-disable-next-line
  handleConflict = (topic, error) => {
    firebase.analytics().logEvent(HTTP_409);
  };

  // eslint-disable-next-line
  handleServer = (topic, error) => {
    firebase.analytics().logEvent(HTTP_5XX);
  };

  // eslint-disable-next-line
  handleNetwork = (topic, error) => {
    firebase.analytics().logEvent(HTTP_NETWORK);
  };

  // eslint-disable-next-line
  handleOffline = (topic, error) => {
    firebase.analytics().logEvent(HTTP_OFFLINE);
  };

  // eslint-disable-next-line
  handleTimeout = (topic, error) => {
    firebase.analytics().logEvent(HTTP_TIMEOUT);
  };


  //* COMPONENT DID CATCH

  componentDidCatch(error) {
    this.handleAppUncaught('UNKNOWN', error);
  }


  //* RENDER

  render() {
    return this.props.children;
  }
}
