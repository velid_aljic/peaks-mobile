import { connect } from 'react-redux';

import { refreshAuthToken } from '@data/session';

import { ErrorsBoundryComponent } from './ErrorsBoundry';


//* MAPPING

const mapStateToProps = (state) => {
  const { authenticated, refreshToken, user } = state.session;

  return {
    authenticated,
    email: user.email,
    refreshToken
  };
};

const mapDispatchToProps = dispatch => ({
  refreshAuthToken: refreshAuthToken(dispatch),
});


//* EXPORT

export const ErrorsBoundry = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ErrorsBoundryComponent);
