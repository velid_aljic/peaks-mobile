import { Toast } from 'native-base';
import React from 'react';

import { APP_INFO, Bus } from '@components/Application/Bus';


export class Info extends React.Component {
  //* LIFECYCLE METHODS

  componentWillMount() {
    // APP INFO
    this.appInfo = Bus.subscribe(APP_INFO, this.handleAppInfo);
  }

  componentWillUnmount() {
    Bus.unsubscribe(this.appInfo);
  }


  //* COMPONENT METHODS

  handleAppInfo = (topic, message) => {
    Toast.show({
      buttonText: 'Ok',
      duration: 10000,
      text: message,
    });
  }


  //* RENDER

  render() {
    return null;
  }
}
