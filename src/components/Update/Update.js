import { AppState } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import NetInfo from '@react-native-community/netinfo';
import React from 'react';


/**
 *
 *  TODO: Remove unnecessary netStatus check in netStatus event listener
 *
 * @name UpdateComponent
 *
 * @prop {Boolean}  authenticated    - Is user authenticated
 * @prop {Boolean}  isSessionSyncing - Is user syncing data with server
 * @prop {String}   languageCode     - User's language code used for i18n
 * @prop {Object}   unsyncedAscents  - Array of unsynced ascents
 * @prop {Object}   version          - Version object
 *
 * @prop {Function} getRankList      - Method for fetching rank list
 *
 * @prop {Function} refreshAscents   - Method for refreshing list of ascents
 *
 * @prop {Function} syncAscents      - Method for syncing ascents with server
 * @arg  {Object}   unsyncedAscents  - Array of unsynced ascents
 *
 * @prop {Function} verifyVersion    - Method for veryfing current version with latest on server
 * @arg  {Object}   currentVersion   - Current version object
 */
export class UpdateComponent extends React.Component {
  state= { isVisible: true }

  //* LIFECYCLE METHODS

  unsubscribeNetInfoToken = null;

  componentDidMount() {
    this.handleActiveState();

    this.unsubscribeNetInfoToken = NetInfo.isConnected.addEventListener('connectionChange',
      (state) => {
        if (state.isConnected) {
          this.handleActiveState();
        }
      });

    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    this.unsubscribeNetInfoToken();
    AppState.removeEventListener('change', this.handleAppStateChange);
  }


  //* COMPONENT METHODS

  // eslint-disable-next-line
  handleAppStateChange = (state) => {
    // state = 'active' || 'background'
    if (state === 'active') {
      this.handleActiveState();
    }
  }

  handleActiveState = () => {
    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected && this.props.authenticated) {
        const {
          isSessionSyncing,
          languageCode,
          unsyncedAscents,
          version,
        } = this.props;

        if (!isSessionSyncing && unsyncedAscents.length) {
          this.props.syncAscents({ unsyncedAscents });
        }

        this.props.verifyVersion({
          languageCode,
          currentVersion: {
            ...version,
            name: DeviceInfo.getVersion()
          },
        });

        this.props.getRankList();
        this.props.refreshAscents();
      }
    });
  }

  //* RENDER

  render() {
    return null;
  }
}
