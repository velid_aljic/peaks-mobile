import { connect } from 'react-redux';

import { getRankList, refreshAscents } from '@data/feed';
import { syncAscents } from '@data/session';
import { verifyVersion } from '@data/preloaded';

import { UpdateComponent } from './Update';


//* MAPING

const mapStateToProps = (state) => {
  const {
    authenticated,
    isSyncing,
    languageCode,
    unsyncedAscents,
  } = state.session;
  const { version } = state.preloaded;

  return {
    authenticated,
    isSessionSyncing: isSyncing,
    languageCode,
    unsyncedAscents,
    version,
  };
};

const mapDispatchToProps = dispatch => ({
  getRankList: getRankList(dispatch),
  refreshAscents: refreshAscents(dispatch),
  syncAscents: syncAscents(dispatch),
  verifyVersion: verifyVersion(dispatch),
});


//* EXPORT

export const Update = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UpdateComponent);
