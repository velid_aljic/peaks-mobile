import { Grid, Row } from 'native-base';
import { StyleSheet } from 'react-native';
import React from 'react';

import { Info } from './Info';
import { Stats } from './Stats';


//* CONSTANTS

// HEADER_HEIGHT = avatar.height + avatar.marginTop + stats.height + stats.marginTop
export const HEADER_HEIGHT = 120 + 0 + 70 + 25;


//* EXPORT

export const Header = ({ info, rank, stats }) => (
  <Grid>
    <Row style={styles.avatar}>
      <Info info={info} rank={rank} />
    </Row>

    <Row style={styles.stats}>
      <Stats stats={stats} />
    </Row>
  </Grid>
);


//* STYLES

const styles = StyleSheet.create({
  avatar: {
    height: 120,
    marginTop: 0,
    width: '100%',
  },

  stats: {
    height: 70,
    marginTop: 30,
    width: '100%',
  }
});
