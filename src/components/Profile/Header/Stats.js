import { Col, Icon, Text } from 'native-base';
import { StyleSheet, View } from 'react-native';
import { human, materialColors } from 'react-native-typography';
import React from 'react';

import { LanguageContext } from '@components/Language';

import labels from '@i18n/Profile.json';


const renderItem = ({
  stats, iconName, iconType, label
}) => (
  <Col>
    <View style={styles.statsView}>
      <Text style={human.title3White}>
        {stats}
      </Text>
      <Icon
        name={iconName}
        type={iconType}
        style={[styles.caption]}
      >
        <Text style={[styles.caption]}>
          {`  ${label.toUpperCase()}`}
        </Text>
      </Icon>
    </View>
  </Col>
);


//* EXPORT

export const Stats = ({ stats }) => (

  <LanguageContext.Consumer>
    {({ languageCode: code }) => (

      <>

        {renderItem({
          stats: stats.peaks,
          iconName: 'flag',
          iconType: 'Foundation',
          label: labels.peak[code]
        })}

        {renderItem({
          stats: stats.ascents,
          iconName: 'hiking',
          iconType: 'FontAwesome5',
          label: labels.ascent[code]
        })}

        {renderItem({
          stats: stats.altitude,
          iconName: 'mountains',
          iconType: 'Foundation',
          label: labels.altitude[code]
        })}

      </>

    )}
  </LanguageContext.Consumer>

);


//* STYLES

const styles = StyleSheet.create({
  statsView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },

  caption: {
    ...human.caption1White,
    color: materialColors.whiteSecondary
  }
});
