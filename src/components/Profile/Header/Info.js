import { ASSETS, DEFAULT_AVATAR } from 'react-native-dotenv';
import {
  Col,
  Icon,
  Text,
  Thumbnail
} from 'native-base';
import { StyleSheet, View } from 'react-native';
import { human } from 'react-native-typography';
import React from 'react';


//* RENDER METHODS

const renderAvatar = picture => (
  <View style={styles.avatarView}>
    <Thumbnail
      style={styles.avatar}
      source={{ uri: picture || `${ASSETS}/${DEFAULT_AVATAR}` }}
    />
  </View>
);

const renderName = ({ name }) => (
  <Text style={[human.title2White, styles.nameLabel]}>
    {name}
  </Text>
);

const renderLocation = ({ location }) => (
  <Icon name="pin" style={[human.headlineWhite, styles.locationLabel]}>
    {`  ${location}`}
  </Icon>
);

const renderRank = ({ altitudeRank }) => (
  <View style={styles.rankView}>
    <Icon name="trophy" style={[human.headlineWhite, styles.rankLabel]}>
      {`  ${altitudeRank}`}
    </Icon>
  </View>
);


//* EXPORT

export const Info = ({ info, rank }) => (
  <>
    <Col size={2}>
      {renderAvatar(info.picture)}
    </Col>

    <Col size={3}>
      <View style={styles.infoView}>
        {renderName({ name: info.name || info.email })}
        {renderLocation({ location: info.location || '-' })}
        {renderRank({ altitudeRank: rank.byAltitude })}
      </View>
    </Col>
  </>
);


//* STYLES

const styles = StyleSheet.create({
  // AVATAR

  avatarView: {
    alignItems: 'center',
    width: '100%',
  },

  avatar: {
    borderColor: '#FFFFFF',
    borderRadius: 60,
    borderWidth: 3,
    height: 120,
    width: 120,
  },

  // INFO VIEW

  infoView: {
    alignItems: 'flex-start',
    height: 120,
    justifyContent: 'center',
    marginRight: 10,
    width: '100%',
  },

  // NAME

  nameLabel: {
    marginTop: 1,
    marginLeft: 3,
    fontSize: 23
  },

  // LOCATION

  locationLabel: {
    marginTop: 3,
    marginLeft: 5
  },

  // RANK

  rankView: {
    flexDirection: 'row',
    alignItems: 'flex-start'
  },

  rankLabel: {
    // fontSize: 12,
    marginLeft: 5,
    marginRight: 0,
    marginTop: 3,
    // opacity: 0.7,
  },

  rankIcon: {
    marginRight: 20,
    marginTop: 4,
  },
});
