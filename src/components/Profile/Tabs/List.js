import { ASSETS } from 'react-native-dotenv';
import {
  Body,
  Left,
  ListItem,
  Text,
  Thumbnail,
} from 'native-base';
import { FlatList, StyleSheet } from 'react-native';
import { human, materialColors } from 'react-native-typography';
import React from 'react';
import moment from 'moment';

import { LanguageContext } from '@components/Language';

import labels from '@i18n/Profile.json';


//* PARSERS

const parseTitle = (peakName, mountainName) => (peakName === mountainName ? peakName : `${peakName}, ${mountainName}`);
const parseSubtitle = (date, altitude) => `${moment(date).format('DD.MM.YYYY')} | ${altitude}m`;


//* LIST ITEM

const Item = ({ item: ascent, goToBadgePreview }) => {
  const { peak, date } = ascent;
  const {
    name,
    mountain,
    badge,
    coordinates
  } = peak;

  const badgePath = `${ASSETS}/${badge}`;

  return (
    <ListItem
      thumbnail
      key={ascent.date}
      onPress={
        () => setTimeout(() => {
          goToBadgePreview({ badge: badgePath, mountainName: peak.mountain });
        }, 0)
        }
    >
      <Left>
        <Thumbnail square source={{ uri: badgePath }} />
      </Left>
      <Body>
        <Text style={[human.subhead]}>{parseTitle(name, mountain)}</Text>
        <Text style={styles.footnote} note numberOfLines={1}>
          {parseSubtitle(date, coordinates.altitude)}
        </Text>
      </Body>
    </ListItem>
  );
};


//* EXPORT

export const ListTab = ({ ascents, goToBadgePreview }) => {
  if (!ascents.length) {
    return (
      <LanguageContext.Consumer>
        {({ languageCode: code }) => (
          <Text style={[human.callout, { textAlign: 'center', marginTop: 70 }]}>{labels['no-ascents'][code]}</Text>
        )}
      </LanguageContext.Consumer>
    );
  }

  const sortedAscents = ascents.sort((a, b) => (`${b.date}`).localeCompare(a.date));

  return (
    <FlatList
      scrollEnabled={false}
      data={sortedAscents}
      initialNumToRender={5}
      keyExtractor={item => item.date}
      renderItem={({ item }) => <Item goToBadgePreview={goToBadgePreview} item={item} />}
    />
  );
};


//* STYLES

const styles = StyleSheet.create({
  footnote: {
    ...human.footnote,
    color: materialColors.blackTertiary
  },
});
