import { ASSETS } from 'react-native-dotenv';
import {
  Col,
  Grid,
  Row,
  Text
} from 'native-base';
import { Image, StyleSheet } from 'react-native';
import { human } from 'react-native-typography';
import React from 'react';


import { LanguageContext } from '@components/Language';


import labels from '@i18n/Profile.json';

import { groupData } from './utils';


//* EXPORT

export const BadgesTab = ({ peaks, goToBadgePreview }) => {
  if (!peaks.length) {
    return (
      <LanguageContext.Consumer>
        {({ languageCode: code }) => (
          <Text style={[human.callout, styles.text]}>{labels['no-badges'][code]}</Text>
        )}
      </LanguageContext.Consumer>
    );
  }

  return (
    <Grid>
      {groupData(peaks).map(group => (
        <Row key={`row-${group[0].id}`} style={styles.row}>
          {group.map((peak) => {
            const badgePath = `${ASSETS}/${peak.badge}`;

            return (
              <Col
                key={peak.id}
                style={styles.container}
                onPress={() => (peak.badge ? goToBadgePreview({ badge: badgePath, mountainName: peak.mountain }) : null)}
              >
                {peak.uuid && (
                <Image
                  style={styles.image}
                  source={{ uri: badgePath }}
                />
                )}
              </Col>
            );
          })}
        </Row>
      ))}
    </Grid>
  );
};


//* STYLES

const styles = StyleSheet.create({
  row: {
    height: 150,
  },

  container: {
    height: 150,
    paddingHorizontal: 8,
  },

  image: {
    flex: 1,
    height: null,
    resizeMode: 'contain',
    width: null,
  },

  text: {
    textAlign: 'center',
    marginTop: 70
  }
});
