//* GROUP UTIL

export const groupData = (input) => {
  if (!input || !input.length) {
    return [];
  }

  const newInput = [...input];

  let arrays = []; // eslint-disable-line
  const size = 3;

  while (newInput.length > 0) {
    arrays.push(newInput.splice(0, size));
  }

  let i = 0;
  while (arrays[arrays.length - 1].length % 3 !== 0) {
    arrays[arrays.length - 1].push({ id: `empty-${i}` });
    i++;
  }

  return arrays;
};
