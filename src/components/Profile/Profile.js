import {
  Icon,
  Tab,
  TabHeading,
  Tabs
} from 'native-base';
import { Image, StyleSheet, View } from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import React from 'react';

import { BadgesTab, ListTab } from './Tabs';
import { HEADER_HEIGHT, Header } from './Header';


//* CONSTANTS

const ACTIVE_COLOR = '#5D95FC';
const INACTIVE_COLOR = '#DCDCDC';

const PARALLAX_BG_IMAGE = require('@assets/grad-bg.png');


/**
 * @name ProfileComponent
 *
 * @prop {Object}   userAscents      - User's ascents
 * @prop {Object}   userInfo         - User's info
 * @prop {Object}   userPeaks        - User's unique peaks
 * @prop {Object}   userRank         - User's rank
 * @prop {Object}   userStats        - User's stats
 *
 * @prop {Function} goToBadgePreview - callback to badge preview page
 */

export class ProfileComponent extends React.Component {
  state = { currentTab: 0 };


  //* RENDER METHODS

  renderTabHeading = (id, icon) => {
    const { currentTab } = this.state;
    const {
      activeText,
      inactiveText,
      tab,
    } = styles;

    return (
      <TabHeading style={tab}>
        <Icon
          name={icon}
          type="Foundation"
          style={currentTab === id ? activeText : inactiveText}
        />
      </TabHeading>
    );
  }

  renderTab = ({ id, icon, Component }) => (
    <Tab heading={this.renderTabHeading(id, icon)}>
      {Component}
    </Tab>
  )

  renderBackground = () => (
    <View style={styles.bgContainer}>
      <Image fadeDuration={200} style={styles.image} source={PARALLAX_BG_IMAGE} />
    </View>
  )


  //* RENDER

  render() {
    const {
      userAscents,
      userInfo,
      userPeaks,
      userRank,
      userStats,
      goToBadgePreview
    } = this.props;

    return (
      <ParallaxScrollView
        parallaxHeaderHeight={HEADER_HEIGHT}
        renderBackground={() => this.renderBackground()}
        renderForeground={() => <Header info={userInfo} rank={userRank} stats={userStats} />}
      >
        <Tabs
          initialPage={0}
          locked
          onChangeTab={({ i }) => this.setState({ currentTab: i })}
          tabBarUnderlineStyle={styles.tabBarUnderline}
          tabContainerStyle={styles.tabContainer}
        >
          { this.renderTab({ id: 0, icon: 'flag', Component: <BadgesTab goToBadgePreview={goToBadgePreview} peaks={userPeaks} /> }) }
          { this.renderTab({ id: 1, icon: 'list', Component: <ListTab goToBadgePreview={goToBadgePreview} ascents={userAscents} /> }) }
        </Tabs>
      </ParallaxScrollView>
    );
  }
}


//* STYLES

const styles = StyleSheet.create({
  // PARALLAX BACKGROUND

  bgContainer: {
    height: HEADER_HEIGHT
  },

  image: {
    flex: 1,
    resizeMode: 'stretch',
  },

  // TAB

  tab: {
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 1,
    borderColor: INACTIVE_COLOR,
  },

  activeText: {
    color: ACTIVE_COLOR,
    fontSize: 20
  },

  inactiveText: {
    color: INACTIVE_COLOR,
    fontSize: 20
  },

  tabBarUnderline: {
    backgroundColor: ACTIVE_COLOR,
    height: 2,
  },

  tabContainer: {
    borderBottomWidth: 0,
    borderColor: INACTIVE_COLOR,
    borderTopWidth: 0,
    elevation: 0,
    height: 40
  }
});
