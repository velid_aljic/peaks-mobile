import { connect } from 'react-redux';

import { updateLanguage } from '@data/session';

import { LanguageComponent } from './Language';


//* MAPPING

const mapStateToProps = (state) => {
  const { languageCode } = state.session;

  return {
    languageCode
  };
};

const mapDispatchToProps = dispatch => ({
  updateLanguage: updateLanguage(dispatch),
});


//* EXPORT

export const Language = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LanguageComponent);
