import 'moment/locale/bs';
import React from 'react';
import moment from 'moment';

import availableLanguages from './languages.json';


//* CONTEXT

export const LanguageContext = React.createContext();


/**
 * @name LanguageComponent
 *
 * @prop {String}    languageCode   -
 *
 * @prop  {Function} updateLanguage - Method to call for language change
 * @arg   {String}   code           - languageCode
 */
export class LanguageComponent extends React.PureComponent {
  componentDidMount() {
    moment.locale(this.props.languageCode);
  }

  updateLanguage = (code) => {
    moment.locale(code);
    this.props.updateLanguage(code);
  }


  //* RENDER

  render() {
    const { languageCode } = this.props;

    return (
      <LanguageContext.Provider value={{
        availableLanguages,
        languageCode,
        updateLanguage: code => this.updateLanguage(code)
      }}
      >
        {this.props.children}
      </LanguageContext.Provider>
    );
  }
}
