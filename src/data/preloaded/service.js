import { get } from '@utils/http';


const API = {
  peaks: '/v1/peaks',
  version: '/v1/versions'
};

export const getPeaksService = () => get(API.peaks);
export const getVersionService = () => get(API.version);
