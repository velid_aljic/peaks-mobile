import { ASSETS } from 'react-native-dotenv';
import { Image } from 'react-native';

import {
  DELETE_MY_ACCOUNT_SUCCESS,

  FB_LOGIN_FAILURE,
  FB_LOGIN_STARTED,
  FB_LOGIN_SUCCESS,

  GOOGLE_LOGIN_FAILURE,
  GOOGLE_LOGIN_STARTED,
  GOOGLE_LOGIN_SUCCESS,

  LOGIN_FAILURE,
  LOGIN_STARTED,
  LOGIN_SUCCESS,

  LOGOUT,

  REFRESH_TOKEN_FAILURE,

  REGISTER_FAILURE,
  REGISTER_STARTED,
  REGISTER_SUCCESS,

  UPDATE_PEAKS,
  UPDATE_PEAKS_FAILURE,
  UPDATE_PEAKS_STARTED,
  UPDATE_PEAKS_SUCCESS,

  VERIFY_VERSION,
  VERIFY_VERSION_FAILURE,
  VERIFY_VERSION_OBSOLETE,
  VERIFY_VERSION_STARTED,
  VERIFY_VERSION_SUCCESS,

} from '@data/shared/actions';
import { branchedActionCreator } from '@data/shared/utils';
import { updateMyRank } from '@data/session/store';

import { asyncActionCreator, asyncDispatcherAdapter } from '@utils/redux';

import { getPeaksService, getVersionService } from './service';
import { verifyVersionDispatcher } from './dispatchers';


// TODO: Refactor prefetch assets logic from reducer to dispacher/middleware


// ACTIONS

const updatePeaksAction = asyncActionCreator(UPDATE_PEAKS);
const verifyVersionAction = branchedActionCreator(VERIFY_VERSION);


// DISPATCHERS

export const updatePeaks = asyncDispatcherAdapter(updatePeaksAction, getPeaksService);
export const verifyVersion = verifyVersionDispatcher(updatePeaks, updateMyRank)(verifyVersionAction, getVersionService);


/**
 * Inital state for PEAKS store
 *
 * @param {Boolean}  isWorking                   - Flag defining wether user is authenticated
 *
 * @param {Object[]} peaks                       - Peaks meta. Contains info about all available peaks.
 * @param {String}   peaks.id                    - Peaks's id
 * @param {String}   peaks.uuid                  - Peak's uuid
 * @param {String}   peaks.name                  - Peaks's name
 * @param {String}   peaks.mountain              - Peaks's mountain
 * @param {String}   peaks.badge                 - Peaks's badge
 * @param {Object}   peaks.coordinates           - Peak's coordinates
 * @param {Number}   peaks.coordinates.altitude  - Peak's altitude
 * @param {Number}   peaks.coordinates.latitude  - Peak's latitude
 * @param {Number}   peaks.coordinates.longitude - Peak's longitude
 * @param {Date}     peaks.createdAt             - Timestamp
 *
 * @param {Object}   version                     - Used to verify if client has latest peaks meta.
 * @param {String}   version.peaksUuid           - Version peak's uuid
 * @param {String}   version.rankUuid            - Version rank's uuid
 */
const initialState = {
  isWorking: false,
  peaks: [],
  version: {
    peaksUuid: null,
    rankUuid: null
  },
};


/**
 * ---------------------------------------------------------------
 * ---------------------------------------------------------------
 * * REDUCER
 * ---------------------------------------------------------------
 * ---------------------------------------------------------------
 *
 * * 1. DELETE_MY_ACCOUNT_SUCCESS / REFRESH_TOKEN_FAILURE / LOGOUT
 * * 2. FB_LOGIN / GOOGLE_LOGIN / LOGIN / REGISTER
 * * 3. VERIFY_VERSION
 * * 4. UPDATE_PEAKS
 */
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 1. DELETE_MY_ACCOUNT_SUCCESS
     * * 1. REFRESH_TOKEN_FAILURE
     * * 1. LOGOUT
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case DELETE_MY_ACCOUNT_SUCCESS:
    case REFRESH_TOKEN_FAILURE:
    case LOGOUT:
      return initialState;


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 2. FB_LOGIN
     * * 2. GOOGLE_LOGIN
     * * 2. LOGIN
     * * 2. REGISTER
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case LOGIN_STARTED:
    case FB_LOGIN_STARTED:
    case GOOGLE_LOGIN_STARTED:
    case REGISTER_STARTED:
      return {
        ...state,
        isWorking: true
      };

    case LOGIN_SUCCESS:
    case FB_LOGIN_SUCCESS:
    case GOOGLE_LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
    {
      const { peaks, version } = action.payload;

      return {
        ...state,
        isWorking: false,
        peaks,
        version
      };
    }

    case LOGIN_FAILURE:
    case FB_LOGIN_FAILURE:
    case REGISTER_FAILURE:
    case GOOGLE_LOGIN_FAILURE:
      return initialState;


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 3. VERIFY VERSION
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case VERIFY_VERSION_STARTED:
      return {
        ...state,
        isWorking: true
      };

    case VERIFY_VERSION_SUCCESS:
    {
      const { newVersion } = action.payload;

      return {
        ...state,
        isWorking: false,
        version: newVersion
      };
    }

    case VERIFY_VERSION_OBSOLETE:
      return {
        ...state,
        isWorking: false
      };

    case VERIFY_VERSION_FAILURE:
      return {
        ...state,
        isWorking: false
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 4. UPDATE PEAKS
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case UPDATE_PEAKS_STARTED:
      return {
        ...state,
        isWorking: true
      };

    case UPDATE_PEAKS_SUCCESS:
    {
      const peaks = action.payload;

      // Prefetch assets
      // FIXME: Move to dispatcher/middleware
      peaks.forEach((peak) => { Image.prefetch(`${ASSETS}/${peak.badge}`); });

      return {
        ...state,
        isWorking: false,
        peaks
      };
    }

    case UPDATE_PEAKS_FAILURE:
      return {
        ...state,
        isWorking: false
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * DEFAULT
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    default:
      return state;
  }
};
