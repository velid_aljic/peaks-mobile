import { omit } from 'lodash';

//* {notify_on_update}
// import { Notifications } from '@components/Notifications';
// import labels from '@i18n/General.json';


/**
 * Dispatch actions according to update version/peaks workflow
 *
* @name verifyVersionDispatcher
*
* @param {*}        upatePeaks
* @param {Actions}  actions
* @param {*}        getVersionService
* @param {Dispatch} dispatch
* @param {Object}   version
*/
export const verifyVersionDispatcher = (updatePeaks, updateMyRank) => (actions, getVersionService) => dispatch => async ({
  currentVersion,
  //* {notify_on_update} languageCode
}) => {
  dispatch(actions.started());

  try {
    const { payload: newVersion } = await getVersionService();

    if (newVersion.peaksUuid === currentVersion.peaksUuid
      && newVersion.rankUuid === currentVersion.rankUuid
      && newVersion.name === currentVersion.name
    ) {
      dispatch(actions.obsolete());
      return Promise.resolve();
    }

    if (newVersion.peaksUuid !== currentVersion.peaksUuid) {
      // Update peaks
      updatePeaks(dispatch)();
    }

    if (newVersion.rankUuid !== currentVersion.rankUuid) {
      // Update my rank
      updateMyRank(dispatch)();
    }

    //* {notify_on_update}
    // if (newVersion.name > currentVersion.name) {
    // Notify user that new version is available
    // Notifications.localNotif({
    // title: labels['update-available'][languageCode],
    // message: newVersion.name
    // });
    // }

    dispatch(actions.success({ payload: { newVersion: omit(newVersion, 'name') } }));
    return Promise.resolve({ newVersion: omit(newVersion, 'name') });
  } catch (error) {
    dispatch(actions.failure({ error }));
    throw error;
  }
};
