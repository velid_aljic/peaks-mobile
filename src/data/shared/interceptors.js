import { actionCreator } from '@utils/redux';

import { UPDATE_AUTH_TOKEN } from './actions';
import { store } from '../store';


// HTTP interceptors for updating authorization token

const AUTH_TOKEN_KEY = 'authorization';

export const onSuccessRequest = (request) => {
  const state = store.getState();
  const { accessToken } = state.session;

  if (accessToken) {
    if (!request.headers) {
      request.headers = {};
    }
    request.headers[AUTH_TOKEN_KEY] = `Bearer ${accessToken}`;
  }
};

export const onSuccessResponse = (response) => {
  if (response.headers) {
    const updateToken = actionCreator(UPDATE_AUTH_TOKEN);
    const action = updateToken({ payload: response.headers[AUTH_TOKEN_KEY] });
    store.dispatch(action);
  }
};
