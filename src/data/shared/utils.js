import { actionCreator } from '@utils/redux';


export const syncAscentsActionCreator = type => ({
  type,
  postpone: actionCreator(`${type}_POSTPONE`),
  started: actionCreator(`${type}_STARTED`),
  success: actionCreator(`${type}_SUCCESS`)
});


export const branchedActionCreator = type => ({
  type,
  failure: actionCreator(`${type}_FAILURE`),
  obsolete: actionCreator(`${type}_OBSOLETE`),
  started: actionCreator(`${type}_STARTED`),
  success: actionCreator(`${type}_SUCCESS`),
});
