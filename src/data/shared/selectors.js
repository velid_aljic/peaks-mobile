import { createSelector } from 'reselect';


export const profileSelector = ({ peaksSelector, userSelector }) => createSelector(
  peaksSelector,
  userSelector,
  (peaks, user) => {
    const { ascents, rank, ...info } = user;

    const userPeaks = peaks.filter(peak => ascents.findIndex(x => x.peakId === peak.id) !== -1);
    const uniqueUserPeaks = [];
    for (let i = 0; i < userPeaks.length; i++) {
      if (uniqueUserPeaks.findIndex(x => x.badge === userPeaks[i].badge) === -1) {
        uniqueUserPeaks.push(userPeaks[i]);
      }
    }

    let userAscents = []; // eslint-disable-line
    let altitude = 0;

    for (let i = 0; i < ascents.length; i++) {
      const targetPeak = peaks.find(peak => peak.id === ascents[i].peakId);

      if (targetPeak) {
        userAscents.push({ peak: { ...targetPeak }, date: ascents[i].date });
        altitude += targetPeak.coordinates.altitude;
      }
    }

    return {
      userAscents,
      userInfo: info,
      userPeaks: uniqueUserPeaks,
      userRank: rank,
      userStats: {
        ascents: ascents.length,
        altitude: `${(altitude / 1000).toFixed(2)} K`,
        peaks: `${userPeaks.length}/${peaks.length}`,
      }
    };
  }
);
