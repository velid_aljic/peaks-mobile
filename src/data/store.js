import {
  applyMiddleware,
  combineReducers,
  compose,
  createStore
} from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';

import { reducer as feedReducer } from './feed';
import { reducer as preloadedReducer } from './preloaded';
import { reducer as sessionReducer } from './session';


const preloadedConfig = {
  key: 'preloaded',
  storage: AsyncStorage,
  blacklist: ['isWorking']
};

const sessionConfig = {
  key: 'session',
  storage: AsyncStorage,
  blacklist: ['isWorking', 'isSyncing']
};

const rootReducer = combineReducers({
  feed: feedReducer,
  preloaded: persistReducer(preloadedConfig, preloadedReducer),
  session: persistReducer(sessionConfig, sessionReducer),
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line

export const store = createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(thunk)),
);

export const persistor = persistStore(store);
