import {
  DELETE_MY_ACCOUNT_SUCCESS,

  FETCH_ASCENTS,
  FETCH_ASCENTS_FAILURE,
  FETCH_ASCENTS_STARTED,
  FETCH_ASCENTS_SUCCESS,

  FETCH_RANK_LIST,
  FETCH_RANK_LIST_FAILURE,
  FETCH_RANK_LIST_STARTED,
  FETCH_RANK_LIST_SUCCESS,

  FETCH_USER,
  FETCH_USER_FAILURE,
  FETCH_USER_STARTED,
  FETCH_USER_SUCCESS,

  LOGOUT,

  REFRESH_ASCENTS,
  REFRESH_ASCENTS_FAILURE,
  REFRESH_ASCENTS_STARTED,
  REFRESH_ASCENTS_SUCCESS,

  REFRESH_TOKEN_FAILURE
} from '@data/shared/actions';

import { asyncActionCreator, asyncDispatcherAdapter, } from '@utils/redux';

import {
  getAscentsService,
  getRankListService,
  getUserService,
  refreshAscentsService
} from './service';


// CONSTANTS

export const DEFAULT_RESPONSE_SIZE = 30;


// ACTIONS

const getAscentsAction = asyncActionCreator(FETCH_ASCENTS);
const getUserAction = asyncActionCreator(FETCH_USER);
const refreshAscentsAction = asyncActionCreator(REFRESH_ASCENTS);
const getRankListAction = asyncActionCreator(FETCH_RANK_LIST);


// DISPATCHERS

export const getAscents = asyncDispatcherAdapter(getAscentsAction, getAscentsService);
export const getUser = asyncDispatcherAdapter(getUserAction, getUserService);
export const refreshAscents = asyncDispatcherAdapter(refreshAscentsAction, refreshAscentsService);
export const getRankList = asyncDispatcherAdapter(getRankListAction, getRankListService);


/**
 * * Initial state for 'feed' store
 *
 * @param {Object}   ascents
 * @param {Object[]} ascents.data
 * @param {Number}   asents.page
 * @param {Boolean}  ascents.hasError
 * @param {Boolean}  ascents.hasLoadedAll
 * @param {Boolean}  ascents.isLoading

 *
 * @param {Object}   rankList
 * @param {Object[]} rankList.data
 * @param {Boolean}  rankList.hasError
 * @param {Boolean}  rankList.isLoading
 *
 * @param {Object}   user
 * @param {Object}   user.data
 * @param {[Object]} user.data.ascents
 * @param {Object}   user.data.rank
 * @param {Date}     user.data.createdAt
 * @param {String}   user.data.email
 * @param {String}   user.data.id
 * @param {String}   user.data.name
 * @param {String}   user.data.picture
 * @param {String}   user.data.role
 * @param {String}   user.id            - User's id used as param for refreshing data
 * @param {Boolean}  user.hasError
 * @param {Boolean}  user.isLoading
 */
const initialState = {
  ascents: {
    data: [],
    page: 1,

    hasError: false,
    hasLoadedAll: false,
    isLoading: false,
  },

  rankList: {
    data: [],
    hasError: false,
    isLoading: true
  },

  user: {
    data: {
      ascents: [],
      rank: {
        byAltitude: null
      }
    },
    id: null,
    hasError: false,
    isLoading: false
  },
};


/**
 * ---------------------------------------------------------------
 * ---------------------------------------------------------------
 * * REDUCER
 * ---------------------------------------------------------------
 * ---------------------------------------------------------------
 *
 * * 1. DELETE_MY_ACCOUNT_SUCCESS / REFRESH_TOKEN_FAILURE / LOGOUT
 * * 2. FETCH_ASCENTS
 * * 3. REFRESH_ASCENTS
 * * 4. FETCH_USER
 * * 5. FETCH_RANK_LIST
 */
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 1. DELETE MY ACCOUNT - success
     * * 1. REFRESH MY TOKEN - failure
     * * 1. LOGOUT
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case DELETE_MY_ACCOUNT_SUCCESS:
    case REFRESH_TOKEN_FAILURE:
    case LOGOUT:
      return initialState;


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 2. FETCH ASCENTS
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case FETCH_ASCENTS_STARTED:
      return {
        ...state,

        ascents: {
          ...state.ascents,

          hasError: false,
          isLoading: true,
        }
      };

    case FETCH_ASCENTS_SUCCESS:
    {
      const { page: currentPage, data } = state.ascents;
      const newData = [...data, ...action.payload];

      const hasLoadedAll = action.payload.length < DEFAULT_RESPONSE_SIZE;
      const page = hasLoadedAll ? currentPage : currentPage + 1;

      return {
        ...state,

        ascents: {
          ...state.ascents,

          data: newData,
          page,

          hasLoadedAll,
          isLoading: false,
        }
      };
    }

    case FETCH_ASCENTS_FAILURE:
    {
      return {
        ...state,

        ascents: {
          ...initialState.ascents,
          hasError: true,
        }
      };
    }


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 3. REFRESH ASCENTS
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case REFRESH_ASCENTS_STARTED:
      return {
        ...state,
        ascents: {
          ...initialState.ascents,
          isLoading: true,
        }
      };


    case REFRESH_ASCENTS_SUCCESS:
    {
      const hasLoadedAll = action.payload.length < DEFAULT_RESPONSE_SIZE;
      const page = hasLoadedAll ? 1 : 2;

      return {
        ...state,
        ascents: {
          ...state.ascents,

          data: action.payload,
          hasLoadedAll,
          isLoading: false,
          page,
        }
      };
    }

    case REFRESH_ASCENTS_FAILURE:
    {
      return {
        ...state,
        ascents: {
          ...initialState.ascents,
          hasError: true,
        }
      };
    }


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 4. FETCH USER
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case FETCH_USER_STARTED:
    {
      const { userId } = action.meta.params;
      return {
        ...state,
        user: {
          data: {
            ascents: [],
            rank: {
              byAltitude: null
            }
          },

          id: userId,

          hasError: false,
          isLoading: true
        }
      };
    }

    case FETCH_USER_SUCCESS:
    {
      const { ascents, ...rest } = action.payload;
      return {
        ...state,
        user: {
          data: {
            ascents,
            ...rest
          },

          id: state.user.id,

          hasError: false,
          isLoading: false,
        }
      };
    }

    case FETCH_USER_FAILURE:
      return {
        ...state,
        user: {
          data: {
            ascents: [],
            rank: {
              byAltitude: null
            }
          },

          id: state.user.id,

          hasError: true,
          isLoading: false,
        }
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 5. FETCH RANK
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case FETCH_RANK_LIST_STARTED:
      return {
        ...state,
        rankList: {
          data: [],
          hasError: false,
          isLoading: true,
        }
      };

    case FETCH_RANK_LIST_SUCCESS:
      return {
        ...state,
        rankList: {
          data: action.payload,
          hasError: false,
          isLoading: false,
        }
      };

    case FETCH_RANK_LIST_FAILURE:
      return {
        ...state,
        rankList: {
          data: [],
          hasError: true,
          isLoading: false,
        }
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * DEFAULT
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    default:
      return state;
  }
};
