import moment from 'moment';


// GROUP BY DATE

export const groupByDate = data => data.reduce((acc, item) => {
  const { date: rawDate } = item;
  const date = moment(rawDate).format('DD-MM-YYYY');

  const i = acc.findIndex(e => e.title === date);
  if (i === -1) {
    acc.push({ title: date, data: [item] });
  } else {
    acc[i].data.push(item);
  }
  return acc;
}, []);
