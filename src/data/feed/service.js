import { compose } from '@utils/url';
import { get } from '@utils/http';


const API = {
  ascents: '/v1/ascents',
  users: '/v1/users/{userId}',
  rank: '/v1/rank'
};


export const getAscentsService = ({ queryParams }) => {
  const url = compose(
    API.ascents,
    {},
    queryParams
  );

  return get(url);
};

export const getUserService = ({ params }) => {
  const { userId } = params;

  const url = compose(
    API.users,
    { userId }
  );

  return get(url);
};

export const refreshAscentsService = () => getAscentsService({ queryParams: { page: 1 } });

export const getRankListService = () => get(API.rank);
