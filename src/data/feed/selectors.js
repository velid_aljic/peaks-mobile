import { createSelector } from 'reselect';

import { profileSelector } from '@data/shared/selectors';

import { groupByDate } from './utils';


const preloadedPeaks = state => state.preloaded.peaks;

const feedAscents = state => state.feed.ascents;
const feedUser = state => state.feed.user.data;

export const ascentsFeedSelector = createSelector(
  feedAscents,
  ascents => ({
    ...ascents,
    data: groupByDate(ascents.data),
  })
);

export const feedProfileSelector = profileSelector({ peaksSelector: preloadedPeaks, userSelector: feedUser });
