import { PermissionsAndroid, Platform } from 'react-native';
import Geolocation from '@react-native-community/geolocation';

import { compose } from '@utils/url';
import { get, post, remove } from '@utils/http';


const API = {
  fbLogin: '/v1/auth/facebook',
  googleLogin: '/v1/auth/google',
  login: '/v1/auth/login',
  refreshToken: '/v1/auth/refresh-token',
  register: '/v1/auth/register',

  myProfile: '/v1/users/profile',
  myRank: '/v1/users/profile/rank',

  ascents: '/v1/ascents',

  users: '/v1/users/{userId}',
};

export const fbLoginService = data => post(API.fbLogin, data);
export const googleLoginService = data => post(API.googleLogin, data);
export const loginService = data => post(API.login, data);
export const refreshTokenService = data => post(API.refreshToken, data);
export const registerService = data => post(API.register, data);

export const getMyProfileService = () => get(API.myProfile);
export const getMyRankService = () => get(API.myRank);

export const postAscentService = data => post(API.ascents, data);

export const deleteMyAccountService = ({ params }) => {
  const { userId } = params;

  const url = compose(
    API.users,
    { userId }
  );

  return remove(url);
};


/**
 * @name getLocationService
 *
 * @apiSuccess {Object} payload
 * @apiSuccess {Object} payload.position
 * @apiSuccess {String} payload.position.timestamp
 * @apiSuccess {Object} payload.position.coords
 * @apiSuccess {String} payload.position.coords.latitude
 * @apiSuccess {String} payload.position.coords.longitude
 * @apiSuccess {String} payload.position.coords.accuracy
 *
 * @apiError {Object} error
 * @apiError {String} error.code
 * @apiError {String} error.message
 */
export const getLocationService = async (callback) => {
  // Request location access permission
  const granted = Platform.OS === 'android'
    ? await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
        title: 'Location Access Required',
      }
    ) : true;


  // Get current location
  if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    Geolocation.getCurrentPosition(
      (position) => {
        callback({
          payload: {
            position
          }
        });
      },
      (error) => {
        callback({ error });
      }
    );
  } else {
    const error = {
      permissionDenied: true,
      message: 'Access to devices location is denied!'
    };
    callback({ error });
  }
};
