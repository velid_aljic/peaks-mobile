import { profileSelector } from '@data/shared/selectors';


const preloadedPeaks = state => state.preloaded.peaks;

const sessionUser = state => state.session.user;

export const sessionProfileSelector = profileSelector({
  peaksSelector: preloadedPeaks,
  userSelector: sessionUser
});
