/**
 * @name measure
 * @description
 * Measures distance in meters between two coords A(alt1, lon1) and B(lat2, lon2)
 *
 * @param {Number} lat1 - A.latitude
 * @param {Number} lon1 - A.longitude
 * @param {Number} lat2 - B.latitude
 * @param {Number} lon2 - B.longitude
 */
export const measure = (lat1, lon1, lat2, lon2) => {
  const R = 6378.137; // Radius of earth in KM
  const dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
  const dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
  + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180)
  * Math.sin(dLon / 2) * Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;
  return d * 1000; // meters
};
