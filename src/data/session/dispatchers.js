import moment from 'moment';

import { APP_INFO, Bus } from '@components/Application/Bus';

import labels from '@i18n/Scan.json';

import { measure } from './utils';


/**
* @name syncAscentsDispatcher
* @description
* Sync user ascents with server. If error during syncing is not 'Forbidden',
* then postpone sycning for later.
*
* @param {Actions}  actions
* @param {*}        postAscentService
* @param {Dispatch} dispatch
* @param {Object}   unsyncedAscents
*/
export const syncAscentsDispatcher = (actions, postAscentService) => dispatch => async ({ unsyncedAscents }) => {
  dispatch(actions.started());

  const promises = unsyncedAscents
    .map(ascent => async () => {
      try {
        const { payload } = await postAscentService(ascent);
        return { payload };
      } catch (error) {
        return { ascent, error };
      }
    });

  let newUnsyncedAscents = []; // eslint-disable-line

  for (let i = 0; i < promises.length; i++) {
    const { ascent, error } = await promises[i](); // eslint-disable-line
    if (error && error !== 'Forbidden') {
      // Postpone ascent sync for later
      newUnsyncedAscents.push(ascent);
    }
  }

  if (newUnsyncedAscents.length) {
    dispatch(actions.postpone({ payload: { newUnsyncedAscents } }));
    return;
  }

  dispatch(actions.success());
};


/**
* @name scanLocationDispatcher
* @description
* Dispatch actions according to location scan workflow
* Try to find peak in local storage that is near to current user's location.
* Ignore ascent if peak was acquired more than once in 24H.
*
* @param {Actions}  actions
* @param {*}        getLocationService
* @param {Dispatch} dispatch
* @param {Object}   ascents
* @param {Object}   peaks
*/
export const scanLocationDispatcher = (actions, getLocationService) => dispatch => async ({ ascents, peaks, languageCode: code }) => {
  dispatch(actions.started());

  getLocationService((response) => {
    if (response.error) {
      if (response.error.permissionDenied) {
        Bus.publish(APP_INFO, labels['scan-permission-denied'][code]);
      } else {
        Bus.publish(APP_INFO, labels['scan-error'][code]);
      }

      dispatch(actions.failure({ error: response.error.message }));
      return;
    }

    const { payload } = response;

    // Determine if current location is near any marked peak
    let nearPeak = null;
    const { latitude: lat1, longitude: lon1, accuracy: r } = payload.position.coords;

    // Current location object
    const location = {
      latitude: lat1,
      longitude: lon1
    };


    // Try to find peak near to current location
    for (let i = 0; i < peaks.length; i++) {
      const { latitude: lat2, longitude: lon2 } = peaks[i].coordinates;

      const distance = measure(lat1, lon1, lat2, lon2);

      if (distance <= r + 400) {
        nearPeak = peaks[i];
        break;
      }
    }


    // If any marked peak is near
    if (nearPeak) {
      const now = new Date();
      const endDate = moment(now);

      // Determine if ascent on the same peak occured in less than 24H
      const lastNearPeakAscent = ascents.find((ascent) => {
        const startDate = moment(ascent.date);
        return (ascent.peakId === nearPeak.id)
        && moment.duration(endDate.diff(startDate)).asHours() < 24;
      });

      if (lastNearPeakAscent) {
        Bus.publish(APP_INFO, labels['scan-obsolete'][code]);
        dispatch(actions.obsolete({ payload: { location } }));
      } else {
        // If not then add new ascent to user
        const newAscent = { peakId: nearPeak.id, date: now.toISOString() };

        Bus.publish(APP_INFO, labels['scan-success'][code]);
        dispatch(actions.success({ payload: { newAscent, location } }));
      }
    } else {
      Bus.publish(APP_INFO, labels['scan-fail'][code]);
      dispatch(actions.obsolete({ payload: { location } }));
    }
  });
};
