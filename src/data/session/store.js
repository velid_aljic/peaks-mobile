
import { GoogleSignin } from 'react-native-google-signin';
import { LoginManager } from 'react-native-fbsdk';

import {
  DELETE_MY_ACCOUNT,
  DELETE_MY_ACCOUNT_FAILURE,
  DELETE_MY_ACCOUNT_STARTED,
  DELETE_MY_ACCOUNT_SUCCESS,

  FB_LOGIN,
  FB_LOGIN_FAILURE,
  FB_LOGIN_STARTED,
  FB_LOGIN_SUCCESS,

  GOOGLE_LOGIN,
  GOOGLE_LOGIN_FAILURE,
  GOOGLE_LOGIN_STARTED,
  GOOGLE_LOGIN_SUCCESS,

  LOGIN,
  LOGIN_FAILURE,
  LOGIN_STARTED,
  LOGIN_SUCCESS,

  LOGOUT,

  REFRESH_MY_PROFILE,
  REFRESH_MY_PROFILE_FAILURE,
  REFRESH_MY_PROFILE_STARTED,
  REFRESH_MY_PROFILE_SUCCESS,

  REFRESH_TOKEN,
  REFRESH_TOKEN_FAILURE,
  REFRESH_TOKEN_STARTED,
  REFRESH_TOKEN_SUCCESS,

  REGISTER,
  REGISTER_FAILURE,
  REGISTER_STARTED,
  REGISTER_SUCCESS,

  SCAN_LOCATION,
  SCAN_LOCATION_FAILURE,
  SCAN_LOCATION_OBSOLETE,
  SCAN_LOCATION_STARTED,
  SCAN_LOCATION_SUCCESS,

  SYNC_ASCENTS,
  SYNC_ASCENTS_POSTPONE,
  SYNC_ASCENTS_STARTED,
  SYNC_ASCENTS_SUCCESS,

  UPDATE_CROSSHAIR,

  UPDATE_LANGUAGE,

  UPDATE_MY_RANK,
  UPDATE_MY_RANK_FAILURE,
  UPDATE_MY_RANK_STARTED,
  UPDATE_MY_RANK_SUCCESS,
} from '@data/shared/actions';
import { branchedActionCreator, syncAscentsActionCreator } from '@data/shared/utils';

import {
  actionCreator,
  asyncActionCreator,
  asyncDispatcherAdapter,
  dispatcherCreator
} from '@utils/redux';

import {
  deleteMyAccountService,
  fbLoginService,
  getLocationService,
  getMyProfileService,
  getMyRankService,
  googleLoginService,
  loginService,
  postAscentService,
  refreshTokenService,
  registerService
} from './service';
import { scanLocationDispatcher, syncAscentsDispatcher } from './dispatchers';


// TODO: Move fb/google logout methods from store to middleware


// ACTIONS

const logoutAction = actionCreator(LOGOUT);
const updateCrosshairAction = actionCreator(UPDATE_CROSSHAIR);
const updateLanguageAction = actionCreator(UPDATE_LANGUAGE);

const deleteMyAccountAction = asyncActionCreator(DELETE_MY_ACCOUNT);
const fbLoginAction = asyncActionCreator(FB_LOGIN);
const googleLoginAction = asyncActionCreator(GOOGLE_LOGIN);
const loginAction = asyncActionCreator(LOGIN);
const refreshMyProfileAction = asyncActionCreator(REFRESH_MY_PROFILE);
const refreshTokenAction = asyncActionCreator(REFRESH_TOKEN);
const registerAction = asyncActionCreator(REGISTER);
const updateMyRankAction = asyncActionCreator(UPDATE_MY_RANK);

const scanLocationAction = branchedActionCreator(SCAN_LOCATION);

const syncAscentsAction = syncAscentsActionCreator(SYNC_ASCENTS);


// DISPATCHERS

export const logout = dispatcherCreator(logoutAction);
export const updateCrosshair = dispatcherCreator(updateCrosshairAction);
export const updateLanguage = dispatcherCreator(updateLanguageAction);

export const deleteMyAccount = asyncDispatcherAdapter(deleteMyAccountAction, deleteMyAccountService);
export const fbLogin = asyncDispatcherAdapter(fbLoginAction, fbLoginService);
export const googleLogin = asyncDispatcherAdapter(googleLoginAction, googleLoginService);
export const login = asyncDispatcherAdapter(loginAction, loginService);
export const refreshMyProfile = asyncDispatcherAdapter(refreshMyProfileAction, getMyProfileService);
export const refreshAuthToken = asyncDispatcherAdapter(refreshTokenAction, refreshTokenService);
export const register = asyncDispatcherAdapter(registerAction, registerService);
export const updateMyRank = asyncDispatcherAdapter(updateMyRankAction, getMyRankService);

export const scanLocation = scanLocationDispatcher(scanLocationAction, getLocationService);

export const syncAscents = syncAscentsDispatcher(syncAscentsAction, postAscentService);


/**
 * Inital state for SESSION store
 *
 * @param {String}   accessToken                 - User's access token
 * @param {Boolean}  authenticated               - Flag defining wether user is authenticated
 * @param {String}   refreshToken                - User's refresh token
 *
 * @param {String}   errorMessage                - Error message
 * @param {Boolean}  hasError                    - Error flag
 * @param {Boolean}  isWorking                   - Is resource busy or fetching data
 * @param {Boolean}  isSyncing                   - Is session data being synced with server
 *
 * @param {String}   languageCode                - Language code identifier - Default 'bs'
 *
 * @param {Object}   crosshairLocation           - Crosshair's location
 * @param {Number}   crosshairLocation.latitude  - Crosshair's latitude
 * @param {Number}   crosshairLocation.longitude - Crosshair's longitude
 *
 * @param {Object}   location                    - User's location
 * @param {Number}   location.latitude           - Location latitude
 * @param {Number}   location.longitude          - Location longitude
 *
 * @param {Object}   user
 * @param {String}   user.id                     - User's id
 * @param {String}   user.name                   - User's name
 * @param {String}   user.email                  - User's email
 * @param {String}   user.picture                - User's picture
 * @param {String}   user.location               - User's location
 * @param {String}   user.role                   - User's role
 * @param {Date}     user.createdAt              - Timestamp
 *
 * @param {[Object]} user.ascents                - Array of user's ascents
 * @param {String}   user.ascents.peakId         - Ascent's peak id
 * @param {Date}     user.ascents.date           - Ascent's date
 *
 * @param {Object}   user.rank                   - User's rank object
 * @param {Number}   user.rank.byAltitude        - User's rank by altitude
 *
 * @param {[Object]} unsyncedAscents             - Array of user's ascents waiting for sync with server

 */
const initialState = {
  accessToken: null,
  authenticated: false,
  refreshToken: null,

  errorMessage: '',
  hasError: false,
  isWorking: false,
  isSyncing: false,

  languageCode: 'bs',

  crosshairLocation: {
    latitude: 43.8524747,
    longitude: 18.424601,
    latitudeDelta: 0.015,
    longitudeDelta: 0.015,
  },
  location: {
    latitude: 43.8524747,
    longitude: 18.424601,
    latitudeDelta: 0.015,
    longitudeDelta: 0.015,
  },

  user: {
    ascents: [],
    rank: {
      byAltitude: null
    },
  },
  unsyncedAscents: [],
};


/**
 * ---------------------------------------------------------------
 * ---------------------------------------------------------------
 * * REDUCER
 * ---------------------------------------------------------------
 * ---------------------------------------------------------------
 *
 * *  1. DELETE_MY_ACCOUNT
 * *  2. FB_LOGIN / GOOGLE_LOGIN / LOGIN / REGISTER
 * *  3. LOGOUT
 * *  4. REFRESH_MY_PROFILE
 * *  5. REFRESH_TOKEN
 * *  6. SCAN_LOCATION
 * *  7. SYNC_ASCENTS
 * *  8. UPDATE_CROSSHAIR
 * *  9. UPDATE_LANGUAGE
 * * 10. UPDATE_MY_RANK
 */

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 1. DELETE MY ACCOUNT
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case DELETE_MY_ACCOUNT_STARTED:
      return {
        ...state,
        isWorking: true
      };

    case DELETE_MY_ACCOUNT_SUCCESS:
    {
      LoginManager.logOut();
      GoogleSignin.signOut();

      return initialState;
    }

    case DELETE_MY_ACCOUNT_FAILURE:
      return {
        ...state,
        isWorking: false,
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 2. FB LOGIN
     * * 2. GOOGLE LOGIN
     * * 2. LOGIN
     * * 2. REGISTER
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case FB_LOGIN_STARTED:
    case GOOGLE_LOGIN_STARTED:
    case LOGIN_STARTED:
    case REGISTER_STARTED:
      return {
        ...state,
        errorMessage: '',
        hasError: false,
        isWorking: true,
      };

    case FB_LOGIN_SUCCESS:
    case GOOGLE_LOGIN_SUCCESS:
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
    {
      const { user, token } = action.payload;
      const { accessToken, refreshToken } = token;

      return {
        ...state,

        accessToken,
        authenticated: true,
        refreshToken,

        isWorking: false,

        user,
        unsyncedAscents: [],
      };
    }

    case LOGIN_FAILURE:
    case FB_LOGIN_FAILURE:
    case GOOGLE_LOGIN_FAILURE:
    case REGISTER_FAILURE:
      return {
        ...initialState,
        hasError: true,
        errorMessage: action.error
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 3. LOGOUT
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case LOGOUT:
    {
      LoginManager.logOut();
      GoogleSignin.signOut();

      return initialState;
    }


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 4. REFRESH MY PROFILE
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case REFRESH_MY_PROFILE_STARTED:
      return {
        ...state,
        isWorking: true
      };

    case REFRESH_MY_PROFILE_SUCCESS:
      return {
        ...state,

        isWorking: false,

        user: {
          ...state.user,
          rank: action.payload.rank
        }
      };

    case REFRESH_MY_PROFILE_FAILURE:
      return {
        ...state,
        isWorking: false,
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 5. REFRESH TOKEN
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case REFRESH_TOKEN_STARTED:
      return {
        ...state,
        isWorking: true,
      };

    case REFRESH_TOKEN_SUCCESS: {
      const { accessToken, refreshToken } = action.payload;

      return {
        ...state,

        accessToken,
        refreshToken,

        isWorking: false,
      };
    }

    case REFRESH_TOKEN_FAILURE:
    {
      LoginManager.logOut();
      GoogleSignin.signOut();

      return initialState;
    }


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 6. SCAN LOCATION
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case SCAN_LOCATION_STARTED:
      return {
        ...state,
        isWorking: true,
      };

    case SCAN_LOCATION_SUCCESS:
    {
      const { user, unsyncedAscents } = state;

      const { newAscent, location } = action.payload;

      const newAscents = [...user.ascents];
      newAscents.push(newAscent);

      const newUnsyncedAscents = [...unsyncedAscents];
      newUnsyncedAscents.push({ ...newAscent, userId: state.user.id });

      return {
        ...state,

        isWorking: false,

        crosshairLocation: location,
        location,

        user: {
          ...state.user,
          ascents: newAscents
        },
        unsyncedAscents: newUnsyncedAscents,
      };
    }

    case SCAN_LOCATION_OBSOLETE:
    {
      const { location } = action.payload;

      return {
        ...state,
        isWorking: false,

        crosshairLocation: location,
        location,
      };
    }

    case SCAN_LOCATION_FAILURE:
      return {
        ...state,
        isWorking: false
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 7. SYNC ASCENTS
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case SYNC_ASCENTS_STARTED:
      return {
        ...state,
        isSyncing: true,
      };

    case SYNC_ASCENTS_SUCCESS:
      return {
        ...state,
        isSyncing: false,
        unsyncedAscents: []
      };

    case SYNC_ASCENTS_POSTPONE:
    {
      const { newUnsyncedAscents } = action.payload;

      return {
        ...state,
        isSyncing: false,
        unsyncedAscents: newUnsyncedAscents
      };
    }


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 8. UPDATE CROSSHAIR
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case UPDATE_CROSSHAIR:
    {
      const {
        latitude,
        longitude,
        latitudeDelta,
        longitudeDelta
      } = action.payload;

      return {

        ...state,
        crosshairLocation: {
          latitude,
          longitude,
          latitudeDelta,
          longitudeDelta
        },
      };
    }


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 9. UPDATE LANGUAGE
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case UPDATE_LANGUAGE:
      return {
        ...state,
        languageCode: action.payload,
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * 10. UPDATE MY RANK
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    case UPDATE_MY_RANK_STARTED:
      return {
        ...state,
        isWorking: true,
      };

    case UPDATE_MY_RANK_SUCCESS:
    {
      const { rank } = action.payload;

      return {
        ...state,

        isWorking: false,

        user: {
          ...state.user,
          rank
        }
      };
    }

    case UPDATE_MY_RANK_FAILURE:
      return {
        ...state,
        isWorking: false
      };


    /**
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     * * DEFAULT
     * ---------------------------------------------------------------
     * ---------------------------------------------------------------
     */
    default:
      return state;
  }
};
