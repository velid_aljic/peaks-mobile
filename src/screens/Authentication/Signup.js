import { ASSETS } from 'react-native-dotenv';
import { Image } from 'react-native';
import { Text, } from 'native-base';
import { connect } from 'react-redux';
import { human } from 'react-native-typography';
import React from 'react';
import firebase from 'react-native-firebase';

import { APP_INFO, Bus } from '@components/Application/Bus';
import { Loader } from '@components/Application/Loader';

import { register } from '@data/session';

import { MAIN_ROUTE, SIGNUP_ROUTE } from '@routes';

import { HEADER_BLUE } from '@shared/Header';

import { SignupView } from './Signup.view';


/**
 * @name SignupPage
 *
 * @prop {Boolean}  authenticated
 * @prop {String}   errorMessage
 * @prop {Boolean}  hasError
 * @prop {Boolean}  isWorking
 * @prop {Object[]} peaks
 */


//* MAPING

const mapStateToProps = (state) => {
  const { peaks } = state.preloaded;
  const {
    authenticated,
    errorMessage,
    hasError,
    isWorking,
  } = state.session;

  return {
    authenticated,
    errorMessage,
    hasError,
    isWorking,
    peaks
  };
};

const mapDispatchToProps = dispatch => ({
  register: register(dispatch),
});


//* CLASS

class SignupComponent extends React.Component {
  static navigationOptions = {
    ...HEADER_BLUE,
    headerTitle: (
      <Text style={human.title1White}>Sign Up</Text>
    )
  }

  state = {
    name: '', email: '', password: ''
  };


  //* LIFECYCLE METHODS

  componentDidMount() {
    firebase.analytics().setCurrentScreen(SIGNUP_ROUTE);
  }

  componentDidUpdate(prevProps) {
    if (this.props.authenticated && !prevProps.authenticated) {
      this.prefetchAssets();
      this.redirect();
    }

    if (this.props.hasError && !prevProps.hasError) {
      Bus.publish(APP_INFO, this.props.errorMessage);
    }
  }


  //* COMPONENT METHODS

  setEmail = ({ email }) => {
    this.setState({ email: email.replace(/\s/g, '') });
  }

  setPassword = ({ password }) => {
    this.setState({ password });
  }

  setName = ({ name }) => {
    this.setState({ name });
  }

  prefetchAssets = () => {
    const { peaks } = this.props;
    peaks.forEach((peak) => { Image.prefetch(`${ASSETS}/${peak.badge}`); });
  }

  redirect = () => {
    this.props.navigation.navigate(MAIN_ROUTE);
  }

  handleSignUp = () => {
    const { name, email, password } = this.state;
    this.props.register({
      name, email, password
    });
  };


  //* RENDER METHODS

  render() {
    const { name, email, password } = this.state;
    const { isWorking } = this.props;

    if (isWorking) {
      return (
        <Loader />
      );
    }

    return (
      <SignupView
        name={name}
        email={email}
        password={password}

        setName={this.setName}
        setEmail={this.setEmail}
        setPassword={this.setPassword}
        handleSignUp={this.handleSignUp}
      />
    );
  }
}


//* EXPORT

export const SignupPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignupComponent);
