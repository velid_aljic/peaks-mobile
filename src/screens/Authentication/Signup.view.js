import {
  Button,
  Container,
  Content,
  Form,
  Grid,
  Icon,
  Input,
  Item,
  Row,
  Text,
} from 'native-base';
import { Image, StatusBar } from 'react-native';
import React from 'react';

import { ACCENT_COLOR, HEADER_COLOR, SECONDARY_COLOR } from '@shared/Theme';

import { styles } from './Login.view';

const BG_IMAGE = require('@assets/grad-bg.png');
const LOGO = require('@assets/logo-white.png');


//* VIEW

export const SignupView = ({
  name,
  email,
  password,

  setName,
  setEmail,
  setPassword,
  handleSignUp
}) => (
  <Container style={styles.container}>
    <StatusBar backgroundColor={HEADER_COLOR} barStyle="light-content" />
    <Image fadeDuration={200} source={BG_IMAGE} style={styles.bgGradient} />

    <Content contentContainerStyle={styles.mainContentContainer} style={styles.mainContent}>
      <Grid>
        <Row style={styles.logoRow} size={1}>
          <Image style={styles.logo} source={LOGO} />
        </Row>

        <Row size={2}>
          <Content scrollEnabled={false}>
            <Form style={styles.form}>
              <Item inlineLabel>
                <Icon
                  active
                  name="user"
                  style={styles.pwIcon}
                  type="FontAwesome"
                />
                <Input
                  autoCapitalize="none"
                  autoCorrect={false}
                  onChangeText={value => setName({ name: value })}
                  placeholder="Name"
                  placeholderTextColor={SECONDARY_COLOR}
                  style={styles.input}
                  value={name}
                />
              </Item>

              <Item inlineLabel>
                <Icon
                  active
                  name="envelope"
                  style={styles.mailIcon}
                  type="FontAwesome"
                />
                <Input
                  autoCapitalize="none"
                  autoCorrect={false}
                  onChangeText={value => setEmail({ email: value.replace(/\s/g, '') })}
                  placeholder="Email"
                  placeholderTextColor={SECONDARY_COLOR}
                  style={styles.input}
                  value={email}
                />
              </Item>

              <Item inlineLabel>
                <Icon
                  name="lock"
                  style={styles.pwIcon}
                  type="FontAwesome"
                />
                <Input
                  autoCapitalize="none"
                  onChangeText={value => setPassword({ password: value })}
                  placeholder="Password"
                  placeholderTextColor={SECONDARY_COLOR}
                  secureTextEntry
                  style={styles.input}
                  value={password}
                />
              </Item>
            </Form>

            <Button
              block
              rounded
              disabled={name === '' || email === '' || password === ''}
              light
              onPress={handleSignUp}
              style={styles.loginBtn}
            >
              <Text style={{ color: ACCENT_COLOR }}>Sign up</Text>
            </Button>
          </Content>
        </Row>
      </Grid>
    </Content>
  </Container>
);
