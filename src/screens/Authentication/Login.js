import { ASSETS, WEB_CLIENT_ID } from 'react-native-dotenv';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import React from 'react';

import { APP_INFO, Bus } from '@components/Application/Bus';
import { Loader } from '@components/Application/Loader';

import { fbLogin, googleLogin, login } from '@data/session/store';

import { MAIN_ROUTE, SIGNUP_ROUTE } from '@routes';

import { LoginView } from './Login.view';


/**
 * @name LoginPage
 *
 * @prop {Boolean}  authenticated
 * @prop {String}   errorMessage
 * @prop {Boolean}  hasError
 * @prop {Boolean}  isWorking
 * @prop {Object[]} peaks
 */


//* MAPING

const mapStateToProps = (state) => {
  const { peaks } = state.preloaded;
  const {
    authenticated,
    errorMessage,
    hasError,
    isWorking,
  } = state.session;

  return {
    authenticated,
    errorMessage,
    hasError,
    isWorking,
    peaks
  };
};

const mapDispatchToProps = dispatch => ({
  fbLogin: fbLogin(dispatch),
  googleLogin: googleLogin(dispatch),
  login: login(dispatch)
});


//* CLASS

class LoginComponent extends React.Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: null,
  };

  state = { email: '', password: '' };


  //* LIFECYCLE METHODS

  componentDidMount() {
    GoogleSignin.configure({
      webClientId: WEB_CLIENT_ID,
      offlineAccess: true,
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.authenticated && !prevProps.authenticated) {
      this.prefetchAssets();
      this.redirect();
    }

    if (this.props.hasError && !prevProps.hasError) {
      Bus.publish(APP_INFO, this.props.errorMessage);
    }
  }


  //* COMPONENT METHODS

  setEmail = ({ email }) => {
    this.setState({ email: email.replace(/\s/g, '') });
  }

  setPassword = ({ password }) => {
    this.setState({ password });
  }

  prefetchAssets = () => {
    const { peaks } = this.props;
    peaks.forEach((peak) => { Image.prefetch(`${ASSETS}/${peak.badge}`); });
  }

  redirect = () => {
    this.props.navigation.navigate(MAIN_ROUTE);
  }

  goToSignup = () => {
    this.props.navigation.navigate(SIGNUP_ROUTE);
  }

  handleLogin = () => {
    const { email, password } = this.state;
    this.props.login({ email, password });
  };

  handleFbLogin = () => {
    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
      (result) => {
        if (result.isCancelled) {
          // console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              this.props.fbLogin({ access_token: data.accessToken.toString() });
            }
          );
        }
      },
      (error) => { // eslint-disable-line
        // console.log(`Login fail with error: ${error}`);
      }
    );
  }

  handleGoogleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      this.props.googleLogin({ access_token: userInfo.idToken });
    } catch (error) {
      // console.log(error);
    }
  };


  //* RENDER

  render() {
    const { email, password } = this.state;
    const { isWorking } = this.props;

    if (isWorking) {
      return (
        <Loader />
      );
    }

    return (
      <LoginView
        email={email}
        password={password}

        setEmail={this.setEmail}
        setPassword={this.setPassword}
        handleLogin={this.handleLogin}
        handleFbLogin={this.handleFbLogin}
        handleGoogleLogin={this.handleGoogleLogin}
        goToSignup={this.goToSignup}
      />
    );
  }
}


//* EXPORT

export const LoginPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginComponent);
