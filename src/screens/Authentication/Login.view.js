import {
  Button,
  Container,
  Content,
  Form,
  Grid,
  Icon,
  Input,
  Item,
  Row,
  Text,
} from 'native-base';
import { Image, StatusBar, StyleSheet } from 'react-native';
import React from 'react';

import { ACCENT_COLOR, HEADER_COLOR, SECONDARY_COLOR } from '@shared/Theme';

const BG_IMAGE = require('@assets/grad-bg.png');
const LOGO = require('@assets/logo-white.png');


//* VIEW

export const LoginView = ({
  email,
  password,

  setEmail,
  setPassword,
  handleLogin,
  handleFbLogin,
  handleGoogleLogin,
  goToSignup
}) => (
  <Container style={styles.container}>
    <StatusBar backgroundColor={HEADER_COLOR} barStyle="light-content" />
    <Image fadeDuration={200} source={BG_IMAGE} style={styles.bgGradient} />

    <Content contentContainerStyle={styles.mainContentContainer} style={styles.mainContent}>
      <Grid>
        <Row style={styles.logoRow} size={1}>
          <Image style={styles.logo} source={LOGO} />
        </Row>

        <Row size={2}>
          <Content scrollEnabled={false}>
            <Form style={styles.form}>
              <Item inlineLabel>
                <Icon
                  active
                  name="envelope"
                  style={styles.mailIcon}
                  type="FontAwesome"
                />
                <Input
                  autoCapitalize="none"
                  autoCorrect={false}
                  onChangeText={value => setEmail({ email: value })}
                  placeholder="Email"
                  placeholderTextColor={SECONDARY_COLOR}
                  style={styles.input}
                  value={email}
                />
              </Item>

              <Item inlineLabel>
                <Icon
                  name="lock"
                  style={styles.pwIcon}
                  type="FontAwesome"
                />
                <Input
                  autoCapitalize="none"
                  onChangeText={value => setPassword({ password: value })}
                  placeholder="Password"
                  placeholderTextColor={SECONDARY_COLOR}
                  secureTextEntry
                  style={styles.input}
                  value={password}
                />
              </Item>
            </Form>

            <Button
              block
              disabled={email === '' || password === ''}
              rounded
              light
              onPress={handleLogin}
              style={styles.loginBtn}
            >
              <Text style={{ color: ACCENT_COLOR }}>Login</Text>
            </Button>

            <Button
              block
              bordered
              rounded
              iconLeft
              light
              onPress={handleFbLogin}
              style={styles.btn}
            >
              <Icon type="FontAwesome" name="facebook" />
              <Text>Sign in with facebook</Text>
            </Button>

            <Button
              block
              bordered
              rounded
              iconLeft
              light
              onPress={handleGoogleLogin}
              style={styles.btn}
            >
              <Icon type="FontAwesome" name="google" />
              <Text>Sign in with google</Text>
            </Button>

            <Button
              block
              bordered
              rounded
              iconLeft
              light
              onPress={goToSignup}
              style={styles.btn}
              uppercase={false}
            >
              <Icon type="FontAwesome" name="envelope" />
              <Text>Sign Up with email</Text>
            </Button>
          </Content>
        </Row>
      </Grid>
    </Content>
  </Container>
);


//* STYLES

export const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
  },

  bgGradient: {
    flex: 1,
    resizeMode: 'stretch',
  },

  mainContent: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },

  mainContentContainer: {
    flex: 1
  },

  logoRow: {
    paddingHorizontal: 60
  },

  logo: {
    flex: 1,
    resizeMode: 'contain',
    height: undefined,
    width: undefined,
  },

  form: {
    paddingLeft: '10%',
    paddingRight: '10%'
  },

  input: {
    color: SECONDARY_COLOR
  },

  mailIcon: {
    color: SECONDARY_COLOR
  },

  pwIcon: {
    color: SECONDARY_COLOR,
    paddingLeft: 5,
    paddingRight: 10
  },

  loginBtn: {
    alignSelf: 'center',
    marginTop: 30,
    width: '80%',
    elevation: 0,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  btn: {
    alignSelf: 'center',
    marginTop: 10,
    width: '80%',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  }
});
