import { createStackNavigator } from 'react-navigation';

import { LOGIN_ROUTE, SIGNUP_ROUTE } from '@routes';

import { LoginPage } from './Login';
import { SignupPage } from './Signup';

export const AuthStack = createStackNavigator({
  [LOGIN_ROUTE]: LoginPage,
  [SIGNUP_ROUTE]: SignupPage
}, {
  initialRouteName: LOGIN_ROUTE
});
