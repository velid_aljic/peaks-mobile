import { connect } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import React from 'react';

import { AuthStack } from './Authentication/Navigator';
import { MainStack } from './Main/Navigator';


//* MAPPING

const mapStateToProps = (state) => {
  const { authenticated } = state.session;

  return {
    authenticated
  };
};


//* CLASS

class EntryComponent extends React.PureComponent {
  render() {
    const { authenticated } = this.props;

    if (authenticated) {
      const Main = createAppContainer(MainStack);
      return <Main />;
    }

    const Auth = createAppContainer(AuthStack);
    return <Auth />;
  }
}


//* EXPORT

export const EntryPage = connect(
  mapStateToProps,
  null,
)(EntryComponent);
