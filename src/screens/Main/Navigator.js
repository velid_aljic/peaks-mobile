import { Icon } from 'native-base';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import React from 'react';

import {
  AVAILABLE_PEAKS_ROUTE,
  BADGE_PREVIEW_ROUTE,
  FEED_PROFILE_ROUTE,
  HOME_ROUTE,
  INSTRUCTIONS_ROUTE,
  MAIN_TAB_ROUTE,
  PROFILE_ROUTE,
  RANK_ROUTE,
  SCAN_ROUTE,
  SETTINGS_ROUTE,
} from '@routes';

import { DARK_BLUE, GRAY } from '@shared/Theme';

import {
  AvailablePeaksPage,
  BadgePreviewPage,
  FeedProfilePage,
  InstructionsPage
} from './Shared';
import { FeedAscentsPage } from './Home';
import { ProfilePage } from './Profile';
import { RankListPage } from './Rank';
import { ScanPage } from './Scan';
import { SettingsStack } from './Settings/Navigator';


//* CONSTANTS

const ACTIVE_COLOR = DARK_BLUE;
const INACTIVE_COLOR = GRAY;

const HOME_ICON = 'home';
const RANK_ICON = 'trophy';
const SCAN_ICON = 'navigate';
const PROFILE_ICON = 'person';
const SETTINGS_ICON = 'settings';


//* EXPORTS

export const MainTabNavigator = createBottomTabNavigator({
  [HOME_ROUTE]: FeedAscentsPage,
  [RANK_ROUTE]: RankListPage,
  [SCAN_ROUTE]: ScanPage,
  [PROFILE_ROUTE]: ProfilePage,
  [SETTINGS_ROUTE]: SettingsStack
}, {

  initialRouteName: PROFILE_ROUTE,

  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;

      const iconName = ((route) => {
        switch (route) {
          case HOME_ROUTE:
            return HOME_ICON;

          case RANK_ROUTE:
            return RANK_ICON;

          case PROFILE_ROUTE:
            return PROFILE_ICON;

          case SETTINGS_ROUTE:
            return SETTINGS_ICON;

          case SCAN_ROUTE:
            return SCAN_ICON;

          default:
            return HOME_ICON;
        }
      })(routeName);

      return (
        <Icon
          style={{ color: focused ? tintColor : INACTIVE_COLOR }}
          name={iconName}
          size={20}
        />
      );
    },
  }),

  tabBarOptions: {
    activeTintColor: ACTIVE_COLOR,
    inactiveTintColor: INACTIVE_COLOR,
    showLabel: false
  },
});


export const MainStack = createStackNavigator({
  [MAIN_TAB_ROUTE]: {
    screen: MainTabNavigator,
    navigationOptions: {
      header: null,
      headerBackTitle: null,
    }
  },
  [FEED_PROFILE_ROUTE]: FeedProfilePage,
  [BADGE_PREVIEW_ROUTE]: BadgePreviewPage,
  [INSTRUCTIONS_ROUTE]: InstructionsPage,
  [AVAILABLE_PEAKS_ROUTE]: AvailablePeaksPage,
});
