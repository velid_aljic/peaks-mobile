import { Content, Text } from 'native-base';
import {
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet
} from 'react-native';
import { human } from 'react-native-typography';
import React from 'react';

import { ProfileComponent } from '@components/Profile';

import labels from '@i18n/Profile.json';

import { HEADER_COLOR } from '@shared/Theme';


//* VIEW

export const ProfileView = ({
  code,
  isFocused,
  isWorking,
  rest,

  refresh,
  goToBadgePreview
}) => (
  <SafeAreaView style={styles.safeViewContainer}>
    <ScrollView
      style={styles.container}
      refreshControl={(
        <RefreshControl
          refreshing={isWorking}
          onRefresh={refresh}
        />
      )}
    >
      {isFocused && <StatusBar backgroundColor={HEADER_COLOR} barStyle="light-content" />}


      <Content style={styles.content}>
        <Text style={[human.title1White, styles.title]}>{labels['my-profile'][code]}</Text>
        <ProfileComponent {...rest} goToBadgePreview={goToBadgePreview} />
      </Content>


    </ScrollView>
  </SafeAreaView>
);


//* STYLES

const styles = StyleSheet.create({

  safeViewContainer: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: HEADER_COLOR
  },

  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },

  content: {
    flex: 1,
    backgroundColor: HEADER_COLOR
  },

  title: {
    paddingLeft: 15,
    paddingBottom: 20,
    backgroundColor: HEADER_COLOR
  }
});
