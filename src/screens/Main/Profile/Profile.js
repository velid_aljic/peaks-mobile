import { connect } from 'react-redux';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';
import { Loader } from '@components/Application/Loader';
import { refreshMyProfile, sessionProfileSelector, syncAscents } from '@data/session';
import { verifyVersion } from '@data/preloaded';

import { BADGE_PREVIEW_ROUTE, PROFILE_ROUTE } from '@routes';

import { ANALYTICS_REFRESH_MY_PROFILE } from '@shared/Analytics';

import { ProfileView } from './Profile.view';

/**
 * @name ProfilePage
 *
 * @prop {Boolean} isLoading   -
 * @prop {Object}  userAscents -
 * @prop {Object}  userInfo    -
 * @prop {Object}  userPeaks   -
 * @prop {Object}  userStats   -
 */


//* MAPPING

const mapStateToProps = (state) => {
  const { isWorking, unsyncedAscents } = state.session;
  const { version } = state.preloaded;


  return {
    ...sessionProfileSelector(state),
    isWorking,
    unsyncedAscents,
    version
  };
};

const mapDispatchToProps = dispatch => ({
  refreshMyProfile: refreshMyProfile(dispatch),
  syncAscents: syncAscents(dispatch),
  verifyVersion: verifyVersion(dispatch)
});


//* COMPONENT

class ProfilePageComponent extends React.Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: null
  }

  //* STATE

  state = { isFocused: false };

  focus = null; // eslint-disable-next-line
  blur = null;


  //* LIFECYCLE METHODS

  componentDidMount() {
    const { id } = this.props.userInfo;
    firebase.analytics().setUserId(id);

    this.focus = this.props.navigation.addListener('didFocus', () => {
      firebase.analytics().setCurrentScreen(PROFILE_ROUTE);

      this.setState({ isFocused: true });
    });

    this.blur = this.props.navigation.addListener('didBlur', () => { this.setState({ isFocused: false }); });
  }

  componentWillUnmount() {
    this.focus.remove();
    this.blur.remove();
  }


  //* COMPONENT METHODS

  refresh = () => {
    firebase.analytics().logEvent(ANALYTICS_REFRESH_MY_PROFILE);

    const { version, unsyncedAscents } = this.props;

    this.props.verifyVersion({ currentVersion: version });


    if (unsyncedAscents.length) {
      this.props.syncAscents({ unsyncedAscents });
    }

    this.props.refreshMyProfile();
  }

  goToBadgePreview = (params) => { this.props.navigation.navigate(BADGE_PREVIEW_ROUTE, { ...params }); }


  //* RENDER

  render() {
    const { isWorking, ...rest } = this.props;
    const { isFocused } = this.state;

    if (isWorking) {
      return <Loader />;
    }

    return (
      <LanguageContext.Consumer>
        {({ languageCode }) => (
          <ProfileView
            code={languageCode}
            isFocused={isFocused}
            isWorking={isWorking}
            rest={rest}

            refresh={this.refresh}
            goToBadgePreview={this.goToBadgePreview}
          />
        )}
      </LanguageContext.Consumer>
    );
  }
}


//* EXPORT

export const ProfilePage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfilePageComponent);
