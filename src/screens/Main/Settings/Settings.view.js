import {
  Body,
  Button,
  Container,
  Content,
  Icon,
  Left,
  ListItem,
  Right,
  Text
} from 'native-base';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet
} from 'react-native';
import { human } from 'react-native-typography';
import React from 'react';

import labels from '@i18n/Settings.json';

import {
  AVAILABLE_PEAKS_ROUTE,
  DELETE_ACCOUNT_ROUTE,
  INSTRUCTIONS_ROUTE,
  LANGUAGE_ROUTE,
} from '@routes';


//* RENDER METHODS

export const renderItem = ({
  action,
  icon,
  iconType = 'Ionicons',
  isLink = true,
  label,
}) => (
  <ListItem icon onPress={action}>
    <Left>
      <Button transparent dark>
        <Icon active name={icon} type={iconType} />
      </Button>
    </Left>
    <Body>
      <Text style={human.subhead}>{label}</Text>
    </Body>
    {isLink && (
    <Right>
      <Icon active name="ios-arrow-forward" />
    </Right>
    )}
  </ListItem>
);


//* VIEW

export const SettingsView = ({
  code,
  peaks,
  isFocused,

  goToRoute,
  goToTerms,
  logout
}) => (
  <SafeAreaView style={styles.safeViewContainer}>
    <Container>
      {isFocused && <StatusBar backgroundColor="#FFF" barStyle="dark-content" />}


      <Content style={styles.content}>
        {/* HEADER */}
        <Text style={[human.title1, styles.title]}>{labels.settings[code]}</Text>

        {/* APP */}
        <ListItem>
          <Text style={human.body}>{labels.application[code].toUpperCase()}</Text>
        </ListItem>

        {renderItem({
          action: () => goToRoute({ route: AVAILABLE_PEAKS_ROUTE, params: { peaks } }),
          icon: 'flag',
          label: labels['available-peaks'][code]
        })}

        {renderItem({
          action: () => goToRoute({ route: LANGUAGE_ROUTE }),
          icon: 'language',
          iconType: 'MaterialIcons',
          label: labels.language[code]
        })}

        {renderItem({
          action: () => goToRoute({ route: INSTRUCTIONS_ROUTE }),
          icon: 'info',
          iconType: 'MaterialIcons',
          label: labels.instructions[code]
        })}

        {renderItem({
          action: goToTerms,
          icon: 'file-document',
          iconType: 'MaterialCommunityIcons',
          label: labels.terms[code]
        })}


        {/* ACCOUNT */}
        <ListItem>
          <Text style={human.body}>{labels.account[code].toUpperCase()}</Text>
        </ListItem>

        {renderItem({
          action: () => goToRoute({ route: DELETE_ACCOUNT_ROUTE }),
          icon: 'account-remove',
          iconType: 'MaterialCommunityIcons',
          label: labels['delete-account'][code]
        })}

        {renderItem({
          action: logout,
          icon: 'exit',
          isLink: false,
          label: labels['sign-out'][code]
        })}

      </Content>

    </Container>
  </SafeAreaView>
);


//* STYLES

const styles = StyleSheet.create({
  safeViewContainer: {
    flex: 1,
    marginTop: 20
  },

  title: {
    marginLeft: 15,
    marginBottom: 20
  }
});
