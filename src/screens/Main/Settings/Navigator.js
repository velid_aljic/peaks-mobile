import { createStackNavigator } from 'react-navigation';

import {
  DELETE_ACCOUNT_ROUTE,
  LANGUAGE_ROUTE,
  SETTINGS_ROUTE,
} from '@routes';

import { DeleteAccountPage } from './DeleteAccount';
import { LanguagePage } from './Language';
import { SettingsPage } from './Settings';


export const SettingsStack = createStackNavigator({
  [DELETE_ACCOUNT_ROUTE]: DeleteAccountPage,
  [LANGUAGE_ROUTE]: LanguagePage,
  [SETTINGS_ROUTE]: SettingsPage
}, {
  initialRouteName: SETTINGS_ROUTE
});
