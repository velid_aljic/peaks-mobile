import {
  Button,
  Container,
  Content,
  Form,
  Icon,
  Input,
  Item,
  Text
} from 'native-base';
import { StatusBar, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { human } from 'react-native-typography';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';

import { deleteMyAccount } from '@data/session';

import labels from '@i18n/Settings.json';

import { ANALYTICS_DELETE_ACCOUNT } from '@shared/Analytics';
import { HEADER_WHITE } from '@shared/Header';


//* CONSTANTS

const DELETE_CODE = 'peaks-bih';


//* MAPPING

const mapStateToProps = (state) => {
  const { user } = state.session;

  return {
    userId: user.id
  };
};

const mapDispatchToProps = dispatch => ({
  deleteMyAccount: deleteMyAccount(dispatch),
});


//* EXPORT

export class DeleteAccountComponent extends React.Component {
  static navigationOptions = {
    ...HEADER_WHITE,
    headerTitle: (
      <LanguageContext.Consumer>
        {({ languageCode: code }) => (
          <Text style={human.title1}>{labels['delete-account'][code]}</Text>
        )}
      </LanguageContext.Consumer>
    )
  }

  state = { deleteCode: '' }


  //* COMPONENT METHODS

  deleteAccount = ({ userId }) => {
    // ANALYTICS
    firebase.analytics().logEvent(ANALYTICS_DELETE_ACCOUNT);

    this.props.deleteMyAccount({ params: { userId } });
  }


  //* RENDER

  render() {
    const { deleteCode } = this.state;
    const { userId } = this.props;

    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />

        <Content style={styles.content}>
          <LanguageContext.Consumer>
            {({ languageCode: code }) => (
              <>

                <Text style={[human.subhead, styles.label]}>
                  <Text>
                    {labels['delete-account-label-1'][code]}
                  </Text>
                  <Text style={styles.bold}>{`${DELETE_CODE} `}</Text>
                  <Text>{labels['delete-account-label-2'][code]}</Text>
                </Text>

                <Form style={styles.form}>
                  <Item regular>
                    <Input
                      onChangeText={value => this.setState({ deleteCode: value })}
                      autoCorrect={false}
                      autoCapitalize="none"
                      placeholder={labels['enter-code'][code]}
                      value={deleteCode}
                      style={human.subhead}
                    />
                  </Item>

                  <Button
                    bordered
                    iconLeft
                    danger
                    onPress={() => this.deleteAccount({ userId })}
                    disabled={this.state.deleteCode !== DELETE_CODE}
                    style={styles.button}
                  >
                    <Icon name="account-remove" type="MaterialCommunityIcons" />
                    <Text>
                      {labels['delete-account'][code]}
                    </Text>
                  </Button>
                </Form>

              </>
            )}
          </LanguageContext.Consumer>

        </Content>
      </Container>
    );
  }
}


//* STYLES

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  content: {
    marginTop: 20,
  },

  label: {
    paddingHorizontal: 20,
  },

  bold: {
    fontWeight: 'bold'
  },

  form: {
    marginTop: 20,
    marginHorizontal: '10%'
  },

  button: {
    marginTop: 20,
    alignSelf: 'center'
  },
});


//* EXPORT

export const DeleteAccountPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeleteAccountComponent);
