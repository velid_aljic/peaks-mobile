
import { Linking } from 'react-native';
import { TERMS } from 'react-native-dotenv';
import { connect } from 'react-redux';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';

import { logout } from '@data/session';

import { ANALYTICS_LOGOUT, ANALYTICS_PREVIEW_TERMS } from '@shared/Analytics';

import { SETTINGS_ROUTE } from '@routes';

import { SettingsView } from './Settings.view';


/**
 * @name SettingsPage
 *
 * @prop {Object} peaks
 */


//* MAPPING

const mapStateToProps = (state) => {
  const { peaks } = state.preloaded;

  return {
    peaks,
  };
};

const mapDispatchToProps = dispatch => ({
  logout: logout(dispatch),
});


//* CLASS

class SettingsComponent extends React.Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: null
  }

  //* STATE

  state = { isFocused: false };

  focus = null; // eslint-disable-next-line
  blur = null;


  //* LIFECYCLE METHODS

  componentDidMount() {
    this.focus = this.props.navigation.addListener('didFocus', () => {
      firebase.analytics().setCurrentScreen(SETTINGS_ROUTE);

      this.setState({ isFocused: true });
    });

    this.blur = this.props.navigation.addListener('didBlur', () => { this.setState({ isFocused: false }); });
  }

  componentWillUnmount() {
    this.focus.remove();
    this.blur.remove();
  }

  //* COMPONENT METHODS

  touchable= (action) => {
    setTimeout(() => {
      action();
    }, 0);
  }

  logout = () => {
    firebase.analytics().logEvent(ANALYTICS_LOGOUT);

    this.touchable(() => this.props.logout());
  }

  goToRoute = ({ route, params }) => {
    firebase.analytics().setCurrentScreen(route);

    this.touchable(() => this.props.navigation.navigate(route, { ...params }));
  }

  goToTerms = () => {
    firebase.analytics().logEvent(ANALYTICS_PREVIEW_TERMS);

    this.touchable(() => Linking.openURL(TERMS).catch((err) => { /* */ })); // eslint-disable-line
  }


  //* RENDER

  render() {
    const { isFocused } = this.state;
    const { peaks } = this.props;

    return (
      <LanguageContext.Consumer>
        {({ languageCode }) => (
          <SettingsView
            code={languageCode}
            peaks={peaks}
            isFocused={isFocused}

            logout={this.logout}
            goToRoute={this.goToRoute}
            goToTerms={this.goToTerms}
          />
        )}
      </LanguageContext.Consumer>
    );
  }
}


//* EXPORT

export const SettingsPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsComponent);
