import {
  Container,
  Content,
  Left,
  List,
  ListItem,
  Radio,
  Right,
  Text
} from 'native-base';
import { StatusBar, StyleSheet } from 'react-native';
import { human } from 'react-native-typography';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';

import labels from '@i18n/Settings.json';

import { ANALYTICS_CHANGE_LANGUAGE } from '@shared/Analytics';
import { HEADER_WHITE } from '@shared/Header';


//* COMPONENT

export class LanguagePage extends React.Component {
  static navigationOptions = {
    ...HEADER_WHITE,
    headerTitle: (
      <LanguageContext.Consumer>
        {({ languageCode: code }) => (
          <Text style={human.title1}>{labels.language[code]}</Text>
        )}
      </LanguageContext.Consumer>
    )
  }


  //* COMPONENT METHODS

  updateLanguage = (languageCode) => {
    // ANALYTICS
    firebase.analytics().logEvent(ANALYTICS_CHANGE_LANGUAGE, { languageCode });

    this.props.updateLanguage(languageCode);
  }


  //* RENDER

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />

        <Content style={styles.content}>
          <List>

            <LanguageContext.Consumer>
              {({ languageCode: code, updateLanguage, availableLanguages }) => availableLanguages.map(language => (
                <ListItem key={language.code} selected={language.code === code} onPress={() => updateLanguage(language.code)}>
                  <Left>
                    <Text style={human.subhead}>{language.name}</Text>
                  </Left>
                  <Right>
                    <Radio
                      color="#C0C0C0"
                      selectedColor="#1E1E1E"
                      selected={language.code === code}
                      onPress={() => updateLanguage(language.code)}
                    />
                  </Right>
                </ListItem>
              ))
              }
            </LanguageContext.Consumer>

          </List>
        </Content>
      </Container>
    );
  }
}


//* STYLES

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  content: {
    flex: 1,
    marginTop: 20,
  },

  title: {
    textAlign: 'center',
    marginTop: 20
  }
});
