import { ASSETS, DEFAULT_AVATAR } from 'react-native-dotenv';
import {
  Body,
  Left,
  ListItem,
  Text,
  Thumbnail,
} from 'native-base';
import { StyleSheet } from 'react-native';
import { human, materialColors } from 'react-native-typography';
import React from 'react';


// TODO: Adjust mapping on backend from _id to id and replace it on frontend


// EXPORT

export class Item extends React.PureComponent {
  render() {
    const {
      _id,
      picture,
      name,
      stats
    } = this.props.item;

    const rank = this.props.index + 1;

    return (
      <ListItem
        avatar
        onPress={
        () => setTimeout(() => {
          this.props.navigate({ userId: _id });
        }, 0)
        }
      >
        <Left>
          <Thumbnail source={{ uri: picture || `${ASSETS}/${DEFAULT_AVATAR}` }} />
        </Left>
        <Body>
          <Text style={[human.subhead]}>{`${rank}. ${name}`}</Text>
          <Text style={styles.footnote} note numberOfLines={1}>
            {`${(stats.totalAltitude / 1000)}K`}
          </Text>
        </Body>
      </ListItem>
    );
  }
}

// STYLES

const styles = StyleSheet.create({
  footnote: {
    ...human.footnote,
    color: materialColors.blackTertiary
  },
});
