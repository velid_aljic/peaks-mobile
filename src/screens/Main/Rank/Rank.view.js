import {
  FlatList,
  RefreshControl,
  SafeAreaView,
  StatusBar,
  StyleSheet
} from 'react-native';
import { Spinner, Text } from 'native-base';
import { human } from 'react-native-typography';
import React from 'react';

import labels from '@i18n/Rank.json';

import { Item } from './Item';


//* RENDER METHODS

const renderItem = ({ item, index }, { goToProfile }) => (
  <Item navigate={goToProfile} item={item} index={index} />
);


//* VIEW

export const RankView = ({
  code,
  data,
  isLoading,
  isFocused,
  hasError,

  goToProfile,
  refreshRankList
}) => (
  <SafeAreaView style={styles.safeViewContainer}>
    {isFocused && <StatusBar backgroundColor="#FFF" barStyle="dark-content" />}

    <>
      <Text style={[human.title1, styles.title]}>{labels['rank-list'][code]}</Text>

      <FlatList
        data={data}
        extraData={data.length}
        keyExtractor={item => item._id} // eslint-disable-line
        renderItem={({ item, index }) => renderItem({ item, index }, { goToProfile })}
        ListFooterComponent={isLoading && <Spinner color="black" />}
        ListHeaderComponent={
          hasError && (
          <Text style={[human.callout, styles.error]}>
            {labels.error[code]}
          </Text>
          )
        }
        refreshControl={(
          <RefreshControl
            refreshing={isLoading}
            onRefresh={refreshRankList}
          />
        )}
      />
    </>

  </SafeAreaView>
);


//* STYLES

const styles = StyleSheet.create({
  safeViewContainer: {
    flex: 1,
    marginTop: 20,
  },

  title: {
    marginLeft: 15,
    marginBottom: 20
  },

  error: {
    paddingHorizontal: 10,
    textAlign: 'center',
  }
});
