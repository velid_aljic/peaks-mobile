
import { connect } from 'react-redux';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';

import { getRankList, getUser } from '@data/feed';

import { FEED_PROFILE_ROUTE, RANK_ROUTE } from '@routes';

import { ANALYTICS_REFRESH_RANK_LIST } from '@shared/Analytics';

import { RankView } from './Rank.view';


// TODO: Adjust mapping on backend from _id to id and replace it on frontend


/**
 * @name RankListPage
 *
 * @prop {Object[]}  data         -
 * @prop {Boolean}   hasError     -
 * @prop {Boolean}   hasLoadedAll -
 * @prop {Boolean}   isLoading    -
 * @prop {Number}    page         -
 */


//* MAPING

const mapStateToProps = (state) => {
  const { data, isLoading, hasError } = state.feed.rankList;

  return {
    data,
    isLoading,
    hasError
  };
};

const mapDispatchToProps = dispatch => ({
  getRankList: getRankList(dispatch),
  getUser: getUser(dispatch),
});


//* CLASS

class RankListComponent extends React.Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: null
  }


  //* STATE

  state = { isFocused: false };

  focus = null; // eslint-disable-next-line
  blur = null;


  //* LIFECYCLE METHODS

  componentDidMount() {
    this.props.getRankList();

    this.focus = this.props.navigation.addListener('didFocus', () => {
      firebase.analytics().setCurrentScreen(RANK_ROUTE);

      this.setState({ isFocused: true });
    });
    this.blur = this.props.navigation.addListener('didBlur', () => { this.setState({ isFocused: false }); });
  }

  componentWillUnmount() {
    this.focus.remove();
    this.blur.remove();
  }


  //* COMPONENT METHODS

  goToProfile = ({ userId }) => {
    this.props.navigation.navigate(FEED_PROFILE_ROUTE);
    this.props.getUser({ params: { userId } });
  }

  refreshRankList = () => {
    firebase.analytics().logEvent(ANALYTICS_REFRESH_RANK_LIST);

    this.props.getRankList();
  }


  //* RENDER

  render() {
    const { data, isLoading, hasError } = this.props;
    const { isFocused } = this.state;

    return (
      <LanguageContext.Consumer>
        {({ languageCode }) => (

          <RankView
            code={languageCode}
            data={data}
            isLoading={isLoading}
            isFocused={isFocused}
            hasError={hasError}

            goToProfile={this.goToProfile}
            refreshRankList={this.refreshRankList}
          />

        )}
      </LanguageContext.Consumer>
    );
  }
}


//* EXPORT

export const RankListPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RankListComponent);
