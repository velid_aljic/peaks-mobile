import { ListItem, Spinner, Text } from 'native-base';
import {
  RefreshControl,
  SafeAreaView,
  SectionList,
  StatusBar,
  StyleSheet
} from 'react-native';
import { human } from 'react-native-typography';
import React from 'react';
import moment from 'moment';

import labels from '@i18n/Home.json';

import { Item } from './Item';


//* RENDER METHODS

const renderHeader = ({ section: { title } }) => {
  const date = moment(title, 'DD-MM-YYYY').format('dddd, Do MMMM, YYYY');
  const capitalized = date.split(' ').map(item => item.charAt(0).toUpperCase() + item.slice(1)).join(' ');

  return (
    <ListItem itemDivider>
      <Text style={human.body}>
        {capitalized}
      </Text>
    </ListItem>
  );
};

const renderItem = ({ item, predicate }, { goToProfile }) => (
  <Item navigate={goToProfile} item={item} predicate={predicate} />);


//* VIEW

export const AscentsView = ({
  code,
  data,
  hasError,
  isFocused,
  isLoading,

  loadMore,
  refresh,
  goToProfile
}) => (
  <SafeAreaView style={styles.safeViewContainer}>
    {isFocused && <StatusBar backgroundColor="#FFF" barStyle="dark-content" />}


    <>
      <Text style={[human.title1, styles.title]}>{labels['ascent-list'][code]}</Text>

      <SectionList
       // stickySectionHeadersEnabled
       // ListEmptyComponent
        sections={data}
        extraData={data.length}
        listkey={item => item.id}
        onEndReachedThreshold={0.2}
        onEndReached={loadMore}
        renderSectionHeader={renderHeader}
        renderItem={({ item }) => renderItem({ item, predicate: labels.climbed[code] }, { goToProfile })}
        ListFooterComponent={isLoading && <Spinner color="black" />}
        ListHeaderComponent={() => {
          if (hasError) {
            return (
              <Text style={[human.callout, styles.error]}>
                {labels.error[code]}
              </Text>
            );
          }

          if (!data.length && !isLoading) {
            return (
              <Text style={[human.callout, styles.error]}>
                {labels['empty-list'][code]}
              </Text>
            );
          }

          return null;
        }

        }
        refreshControl={(
          <RefreshControl
            refreshing={isLoading}
            onRefresh={refresh}
          />
        )}
      />
    </>


  </SafeAreaView>
);


//* STYLES

const styles = StyleSheet.create({
  safeViewContainer: {
    flex: 1,
    marginTop: 20,
  },

  title: {
    marginLeft: 15,
    marginBottom: 20
  },

  error: {
    paddingHorizontal: 20,
    textAlign: 'center',
  }
});
