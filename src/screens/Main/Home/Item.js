import { ASSETS, DEFAULT_AVATAR } from 'react-native-dotenv';
import {
  Body,
  Left,
  ListItem,
  Text,
  Thumbnail,
} from 'native-base';
import { StyleSheet } from 'react-native';
import { human, materialColors } from 'react-native-typography';
import React from 'react';


//* METHODS

const parseSubTitle = (peakName, mountainName) => (peakName === mountainName ? peakName : `${peakName}, ${mountainName}`);


//* EXPORT

export class Item extends React.PureComponent {
  render() {
    const { predicate, item } = this.props;
    const { userId } = item;
    const { user, peak } = item.details;

    return (
      <ListItem
        thumbnail
        onPress={
        () => setTimeout(() => {
          this.props.navigate({ userId });
        }, 0)
        }
      >
        <Left>
          <Thumbnail source={{ uri: user.picture || `${ASSETS}/${DEFAULT_AVATAR}` }} />
        </Left>
        <Body>
          <Text style={human.subhead}>{`${user.name} ${predicate} ${peak.name}`}</Text>
          <Text style={styles.footnote} note numberOfLines={1}>{parseSubTitle(peak.name, peak.mountain)}</Text>
        </Body>
      </ListItem>
    );
  }
}


//* STYLES

const styles = StyleSheet.create({
  footnote: {
    ...human.footnote,
    color: materialColors.blackTertiary
  },
});
