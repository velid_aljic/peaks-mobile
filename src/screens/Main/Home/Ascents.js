import { connect } from 'react-redux';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';

import {
  ascentsFeedSelector,
  getAscents,
  getUser,
  refreshAscents
} from '@data/feed';

import { ANALYTICS_LOAD_MORE_ASCENTS, ANALYTICS_REFRESH_ASCENTS_LIST } from '@shared/Analytics';

import { FEED_PROFILE_ROUTE, HOME_ROUTE } from '@routes';

import { AscentsView } from './Ascents.view';


/**
 * @name FeedAscentsPage
 *
 * @prop {Object[]}  data         -
 * @prop {Boolean}   hasError     -
 * @prop {Boolean}   hasLoadedAll -
 * @prop {Boolean}   isLoading    -
 * @prop {Number}    page         -
 */


//* MAPING

const mapStateToProps = state => ({
  ...ascentsFeedSelector(state)
});

const mapDispatchToProps = dispatch => ({
  getAscents: getAscents(dispatch),
  getUser: getUser(dispatch),
  refreshAscents: refreshAscents(dispatch)
});


//* CLASS

class FeedAscentsComponent extends React.Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: null,
  }

  //* STATE

  state = { isFocused: false };

  focus = null; // eslint-disable-next-line
  blur = null;


  //* LIFECYCLE METHODS

  componentDidMount() {
    const { page } = this.props;
    this.props.getAscents({ queryParams: { page } });

    this.focus = this.props.navigation.addListener('didFocus', () => {
      // ANALYTICS
      firebase.analytics().setCurrentScreen(HOME_ROUTE);

      this.setState({ isFocused: true });
    });

    this.blur = this.props.navigation.addListener('didBlur', () => { this.setState({ isFocused: false }); });
  }

  componentWillUnmount() {
    this.focus.remove();
    this.blur.remove();
  }


  //* COMPONENT METHODS

  refresh = () => {
    firebase.analytics().logEvent(ANALYTICS_REFRESH_ASCENTS_LIST);

    this.props.refreshAscents();
  }

  loadMore = () => {
    firebase.analytics().logEvent(ANALYTICS_LOAD_MORE_ASCENTS);

    const {
      hasError,
      hasLoadedAll,
      isLoading,
      page,
    } = this.props;

    if (!isLoading && !hasLoadedAll && !hasError) {
      this.props.getAscents({ queryParams: { page } });
    }
  }

  goToProfile = ({ userId }) => {
    this.props.navigation.navigate(FEED_PROFILE_ROUTE);
    this.props.getUser({ params: { userId } });
  }


  //* RENDER

  render() {
    const { data, isLoading, hasError } = this.props;
    const { isFocused } = this.state;

    return (
      <LanguageContext.Consumer>
        {({ languageCode }) => (

          <AscentsView
            code={languageCode}
            data={data}
            hasError={hasError}
            isFocused={isFocused}
            isLoading={isLoading}

            loadMore={this.loadMore}
            refresh={this.refresh}
            goToProfile={this.goToProfile}
          />

        )}
      </LanguageContext.Consumer>
    );
  }
}


//* EXPORT

export const FeedAscentsPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(FeedAscentsComponent);
