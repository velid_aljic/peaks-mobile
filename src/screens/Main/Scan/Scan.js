import { connect } from 'react-redux';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';

import { scanLocation, updateCrosshair } from '@data/session';

import { SCAN_ROUTE } from '@routes';

import { ANALYTICS_SCAN_LOCATION } from '@shared/Analytics';

import { ScanView } from './Scan.view';


//* MAPING

const mapStateToProps = (state) => {
  const {
    crosshairLocation,
    isWorking,
    languageCode,
    location,
    user,
  } = state.session;
  const { peaks } = state.preloaded;

  return {
    ascents: user.ascents,
    crosshairLocation,
    isWorking,
    languageCode,
    location,
    peaks,
  };
};

const mapDispatchToProps = dispatch => ({
  scanLocation: scanLocation(dispatch),
  updateCrosshair: updateCrosshair(dispatch)
});


//* CLASS

class ScanComponent extends React.Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: null
  }

  state = { isFocused: false, isFabActive: false };

  focus = null; // eslint-disable-next-line
  blur = null;

  //* LIFECYCLE METHODS

  componentDidMount() {
    this.focus = this.props.navigation.addListener('didFocus', () => {
      firebase.analytics().setCurrentScreen(SCAN_ROUTE);

      this.setState({ isFocused: true });
    });

    this.blur = this.props.navigation.addListener('didBlur', () => {
      this.setState({ isFocused: false });
    });
  }

  componentWillUnmount() {
    this.focus.remove();
    this.blur.remove();
  }


  //* COMPONENT METHODS

  scanLocation = () => {
    firebase.analytics().logEvent(ANALYTICS_SCAN_LOCATION);

    const { ascents, peaks, languageCode } = this.props;
    this.props.scanLocation({ ascents, peaks, languageCode });
  }

  updateCrosshair = (position) => {
    this.props.updateCrosshair({ ...position });
  }

  toggleFab = () => {
    const { isFabActive } = this.state;
    this.setState({ isFabActive: !isFabActive });
  }

  goToRoute = ({ route, params }) => {
    firebase.analytics().setCurrentScreen(route);

    this.props.navigation.navigate(route, { ...params });
  }


  //* RENDER

  render() {
    const {
      crosshairLocation,
      isWorking,
      location,
      peaks,
    } = this.props;
    const { isFocused, isFabActive } = this.state;

    return (
      <LanguageContext.Consumer>
        {({ languageCode }) => (
          <ScanView
            code={languageCode}
            crosshairLocation={crosshairLocation}
            isFocused={isFocused}
            isFabActive={isFabActive}
            isWorking={isWorking}
            location={location}
            peaks={peaks}

            updateCrosshair={this.updateCrosshair}
            scanLocation={this.scanLocation}
            toggleFab={this.toggleFab}
            goToRoute={this.goToRoute}
          />
        )}
      </LanguageContext.Consumer>
    );
  }
}


//* EXPORT

export const ScanPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScanComponent);
