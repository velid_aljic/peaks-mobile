import {
  Button,
  Container,
  Fab,
  Icon,
  Text
} from 'native-base';
import {
  Image,
  StatusBar,
  StyleSheet,
  View
} from 'react-native';
import { human } from 'react-native-typography';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import React from 'react';

import labels from '@i18n/Scan.json';

import { AVAILABLE_PEAKS_ROUTE, INSTRUCTIONS_ROUTE } from '@routes';

import mapStyle from './mapStyle';

const CROSSHAIR = require('@assets/cross-shaped-target.png');
const HIKER = require('@assets/hiking.png');
const FLAG = require('@assets/achievement.png');

const LATITUDE_DELTA = 0.015;
const LONGITUDE_DELTA = 0.015;


//* VIEW

export const ScanView = ({
  code,
  crosshairLocation,
  isFocused,
  isFabActive,
  isWorking,
  location,
  peaks,

  updateCrosshair,
  scanLocation,
  toggleFab,
  goToRoute
}) => (
  <Container>

    {isFocused && <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />}


    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE}
        customMapStyle={mapStyle}
        style={styles.map}
        onRegionChangeComplete={position => updateCrosshair({ ...position })}
        region={{
          latitude: crosshairLocation.latitude,
          longitude: crosshairLocation.longitude,
          latitudeDelta: crosshairLocation.latitudeDelta || LATITUDE_DELTA,
          longitudeDelta: crosshairLocation.longitudeDelta || LONGITUDE_DELTA,
        }}
      >
        <>
          {peaks.map((peak) => {
            const { coordinates, mountain, name } = peak;

            return (
              <Marker
                key={`${name}.${mountain}`}
                title={name === mountain ? name : `${name}, ${mountain}`}
                description={`${coordinates.altitude.toString()} m`}
                coordinate={{
                  latitude: coordinates.latitude,
                  longitude: coordinates.longitude
                }}
              >
                <Image style={styles.markerImage} source={FLAG} />
              </Marker>
            );
          })}

          <Marker
            title={labels['my-location'][code]}
            coordinate={{
              latitude: location.latitude,
              longitude: location.longitude
            }}
          >
            <Image style={styles.markerImage} source={HIKER} />
          </Marker>
        </>
      </MapView>

      <View style={styles.crosshairContainer}>
        <Image style={styles.crosshair} source={CROSSHAIR} />
      </View>

      <View style={styles.scanBtnContainer}>
        <Button
          iconLeft
          rounded
          style={isWorking ? styles.busyBtn : styles.btn}
          disabled={isWorking}
          onPress={() => scanLocation()}
        >
          <Icon name="pin" />
          <Text style={[human.headlineWhite]}>
            {isWorking ? labels['scan-busy'][code] : labels['scan-location'][code]}
          </Text>
        </Button>
      </View>

      <Fab
        active={isFabActive}
        direction="down"
        containerStyle={styles.infoBtnContainer}
        style={styles.fabBtn}
        position="bottomRight"
        onPress={() => toggleFab()}
      >
        <Icon name={isFabActive ? 'ios-arrow-up' : 'ios-arrow-down'} />
        <Button
          onPress={() => goToRoute({ route: AVAILABLE_PEAKS_ROUTE, params: { peaks } })}
          style={styles.fabBtn}
        >
          <Icon name="flag" />
        </Button>
        <Button
          onPress={() => goToRoute({ route: INSTRUCTIONS_ROUTE })}
          style={styles.fabBtn}
        >
          <Icon type="SimpleLineIcons" name="info" />
        </Button>
      </Fab>

    </View>
  </Container>
);


//* STYLES

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  map: {
    ...StyleSheet.absoluteFillObject,
  },

  scanBtnContainer: {
    marginBottom: 30,
    flexDirection: 'row'
  },

  btn: {
    backgroundColor: '#fe6131',
    elevation: 0,
  },

  busyBtn: {
    elevation: 0,
  },

  infoBtnContainer: {
    position: 'absolute',
    top: 30,
    right: 15,
  },

  fabBtn: {
    backgroundColor: '#2F4858'
  },

  crosshairContainer: {
    left: '50%',
    marginLeft: -12,
    marginTop: -24,
    position: 'absolute',
    top: '50%'
  },

  crosshair: {
    width: 24,
    height: 24
  },

  markerImage: {
    width: 36,
    height: 36
  }
});
