import { ASSETS } from 'react-native-dotenv';
import {
  Body,
  Container,
  Content,
  Left,
  List,
  ListItem,
  Text,
  Thumbnail
} from 'native-base';
import { StatusBar, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { human } from 'react-native-typography';
import React from 'react';

import { LanguageContext } from '@components/Language';


import { updateCrosshair } from '@data/session';

import labels from '@i18n/Settings.json';

import { SCAN_ROUTE } from '@routes';

import { HEADER_WHITE } from '@shared/Header';


//* PARSERS

const parseTitle = (peakName, mountainName) => (peakName === mountainName ? peakName : `${peakName}, ${mountainName}`);
const parseSubtitle = altitude => `${altitude}m`;


//* MAPPING

const mapDispatchToProps = dispatch => ({
  updateCrosshair: updateCrosshair(dispatch),
});


/**
 * Navigation params:
 * @prop {Object[]} peaks
 */

//* COMPONENT

export class AvailablePeaksComponent extends React.Component {
  static navigationOptions = {
    ...HEADER_WHITE,
    headerTitle: (
      <LanguageContext.Consumer>
        {({ languageCode: code }) => (
          <Text style={human.title1}>{labels['available-peaks'][code]}</Text>
        )}
      </LanguageContext.Consumer>
    )
  }

  //* COMPONENT METHODS

  previewPeakOnMap = ({ coordinates }) => {
    const { latitude, longitude } = coordinates;
    this.props.updateCrosshair({ latitude, longitude });

    setTimeout(() => {
      this.props.navigation.navigate(SCAN_ROUTE);
    }, 0);
  }


  //* RENDER

  render() {
    const { navigation } = this.props;
    const peaks = navigation.getParam('peaks', []);

    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />

        <Content style={styles.content}>
          <List>
            {peaks.map((peak) => {
              const {
                name,
                badge,
                mountain,
                coordinates
              } = peak;

              return (
                <ListItem thumbnail onPress={() => this.previewPeakOnMap({ coordinates })} key={name}>
                  <Left>
                    <Thumbnail square source={{ uri: `${ASSETS}/${badge}` }} />
                  </Left>
                  <Body>
                    <Text style={[human.subhead]}>{parseTitle(name, mountain)}</Text>
                    <Text style={styles.footnote} note numberOfLines={1}>
                      {parseSubtitle(coordinates.altitude)}
                    </Text>
                  </Body>
                </ListItem>
              );
            })}
          </List>
        </Content>
      </Container>
    );
  }
}


//* STYLES

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  content: {
    flex: 1,
    marginTop: 20,
  },

  title: {
    textAlign: 'center',
    marginTop: 20
  }
});


//* EXPORT

export const AvailablePeaksPage = connect(
  null,
  mapDispatchToProps,
)(AvailablePeaksComponent);
