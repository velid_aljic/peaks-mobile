import {
  Container,
  Grid,
  Row,
  Text,
} from 'native-base';
import {
  Image,
  StatusBar,
  StyleSheet,
  View
} from 'react-native';
import { human } from 'react-native-typography';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';

import labels from '@i18n/Badge.json';

import { HEADER_WHITE } from '@shared/Header';

import { BADGE_PREVIEW_ROUTE } from '@routes';


/**
 * @name BadgePreviewPage
 *
 * Navigation params:
 * @prop {String} badge - server path for badge asset
 */


//* EXPORT

export class BadgePreviewPage extends React.PureComponent {
  static navigationOptions = {
    ...HEADER_WHITE,
    headerTitle: (
      <LanguageContext.Consumer>
        {({ languageCode: code }) => (
          <Text style={human.title1}>{labels.badge[code]}</Text>
        )}
      </LanguageContext.Consumer>
    )
  }


  //* LIFECYCLE METHODS

  componentDidMount() {
    // ANALYTICS
    firebase.analytics().setCurrentScreen(BADGE_PREVIEW_ROUTE);
  }


  //* RENDER

  render() {
    const { navigation } = this.props;
    const badge = navigation.getParam('badge', '-');
    const mountainName = navigation.getParam('mountainName', '-');

    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />

        <Grid>
          <Row style={styles.imageRow} size={2}>
            <Image
              style={styles.image}
              source={{ uri: badge }}
            />
          </Row>
          <Row size={1}>
            <View style={styles.titleView}>
              <Text style={[human.title2, styles.title]}>{mountainName}</Text>
            </View>
          </Row>
        </Grid>
      </Container>
    );
  }
}


//* STYLES

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  imageRow: {
    paddingHorizontal: 20
  },

  image: {
    flex: 1,
    height: undefined,
    width: undefined,
    resizeMode: 'contain',
  },

  titleView: {
    flex: 1,
    textAlign: 'center',
    alignItems: 'center'
  },

  title: {
    textAlign: 'center',
    marginTop: 20,
  }
});
