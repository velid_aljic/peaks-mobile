import {
  Container,
  Content,
  Text
} from 'native-base';
import {
  Linking,
  StatusBar,
  StyleSheet
} from 'react-native';
import { human } from 'react-native-typography';
import React from 'react';

import { LanguageContext } from '@components/Language';

import labels from '@i18n/Instructions.json';

import { HEADER_WHITE } from '@shared/Header';


//* COMPONENT

export class InstructionsPage extends React.Component {
  static navigationOptions = {
    ...HEADER_WHITE,
    headerTitle: (
      <LanguageContext.Consumer>
        {({ languageCode: code }) => (
          <Text style={human.title1}>{labels.instructions[code]}</Text>
        )}
      </LanguageContext.Consumer>
    )
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />

        <Content style={styles.content}>
          <LanguageContext.Consumer>
            {({ languageCode: code }) => (
              <>
                <Text style={[human.subhead, styles.paragraph]}>
                  {labels.paragraph_1[code]}
                </Text>

                <Text style={[human.subhead, styles.paragraph]}>
                  {labels.paragraph_2[code]}
                </Text>

                <Text style={[human.subhead, styles.paragraph]}>
                  {labels.paragraph_3_1[code]}
                  {'\n'}
                  {labels.paragraph_3_2[code]}
                  {'\n'}
                  {labels.paragraph_3_3[code]}
                  {'\n'}
                  {labels.paragraph_3_4[code]}
                </Text>

                <Text style={[human.subhead, styles.paragraph]}>
                  {labels.paragraph_4[code]}
                </Text>

                <Text style={[human.subhead, styles.paragraph]}>
                  <Text style={human.subhead}>
                    {labels.paragraph_5[code]}
                  </Text>
                  <Text
                    style={[human.subhead, { color: '#5A92FF' }]}
                    onPress={() => Linking.openURL('mailto:hello@peaks.ba')}
                  >
                    {' hello@peaks.ba'}
                  </Text>
                </Text>

                <Text style={[human.subhead, styles.paragraph]}>
                  {labels.paragraph_6[code]}
                </Text>
              </>
            )}
          </LanguageContext.Consumer>

        </Content>
      </Container>
    );
  }
}


//* STYLES

const styles = StyleSheet.create({
  content: {
    marginTop: 20
  },

  paragraph: {
    paddingHorizontal: 25,
    marginBottom: 20
  }
});
