import { Text } from 'native-base';
import { connect } from 'react-redux';
import { human } from 'react-native-typography';
import React from 'react';
import firebase from 'react-native-firebase';

import { LanguageContext } from '@components/Language';
import { Loader } from '@components/Application/Loader';

import { feedProfileSelector, getUser } from '@data/feed';

import labels from '@i18n/Profile.json';

import { BADGE_PREVIEW_ROUTE, FEED_PROFILE_ROUTE } from '@routes';

import { ANALYTICS_REFRESH_FEED_PROFILE } from '@shared/Analytics';
import { HEADER_BLUE } from '@shared/Header';

import { FeedErrorView, FeedProfileView } from './Profile.view';


/**
 * @name FeedProfilePage
 *
 * @prop {Boolean} isLoading   -
 * @prop {Object}  userAscents -
 * @prop {Object}  userInfo    -
 * @prop {Object}  userPeaks   -
 * @prop {Object}  userStats   -
 */


//* MAPPING

const mapStateToProps = (state) => {
  const { id, isLoading, hasError } = state.feed.user;

  return {
    ...feedProfileSelector(state),
    userId: id,
    isLoading,
    hasError,
  };
};

const mapDispatchToProps = dispatch => ({
  getUser: getUser(dispatch)
});


//* COMPONENT

class FeedProfileComponent extends React.Component {
  static navigationOptions = {
    ...HEADER_BLUE,
    headerTitle: (
      <LanguageContext.Consumer>
        {({ languageCode: code }) => (
          <Text style={human.title1White}>{labels.profile[code]}</Text>
        )}
      </LanguageContext.Consumer>
    )
  }


  //* LIFECYCLE METHODS

  componentDidMount() {
    firebase.analytics().setCurrentScreen(FEED_PROFILE_ROUTE);
  }


  //* COMPONENT METHODS

  refresh = () => {
    firebase.analytics().logEvent(ANALYTICS_REFRESH_FEED_PROFILE);

    const { userInfo, userId } = this.props;
    this.props.getUser({ params: { userId: userInfo.id || userId } });
  }

  goToBadgePreview = (params) => { this.props.navigation.navigate(BADGE_PREVIEW_ROUTE, { ...params }); }


  //* RENDER

  render() {
    const { isLoading, hasError, ...rest } = this.props;

    if (hasError) {
      return (
        <LanguageContext.Consumer>
          {({ languageCode }) => (
            <FeedErrorView
              code={languageCode}
              isLoading={isLoading}

              refresh={this.refresh}
            />
          )}
        </LanguageContext.Consumer>
      );
    }

    if (isLoading) {
      return <Loader />;
    }

    return (
      <FeedProfileView
        isLoading={isLoading}
        rest={rest}

        refresh={this.refresh}
        goToBadgePreview={this.goToBadgePreview}
      />
    );
  }
}


//* EXPORT

export const FeedProfilePage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(FeedProfileComponent);
