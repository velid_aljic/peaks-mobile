import {
  RefreshControl,
  ScrollView,
  StatusBar,
  StyleSheet
} from 'react-native';
import { Text } from 'native-base';
import { human } from 'react-native-typography';
import React from 'react';

import { ProfileComponent } from '@components/Profile';

import labels from '@i18n/Profile.json';

import { HEADER_COLOR } from '@shared/Theme';


//* VIEW

export const FeedProfileView = ({
  rest,
  isLoading,

  refresh,
  goToBadgePreview
}) => (
  <ScrollView
    refreshControl={(<RefreshControl refreshing={isLoading} onRefresh={refresh} />)}
    style={styles.container}
  >
    <StatusBar backgroundColor={HEADER_COLOR} barStyle="light-content" />
    <ProfileComponent {...rest} goToBadgePreview={goToBadgePreview} />
  </ScrollView>
);


//* ERROR

export const FeedErrorView = ({
  code,
  isLoading,

  refresh
}) => (
  <ScrollView
    refreshControl={(<RefreshControl refreshing={isLoading} onRefresh={refresh} />)}
    style={styles.container}
  >
    <StatusBar backgroundColor={HEADER_COLOR} barStyle="light-content" />
    <Text style={[human.callout, styles.error]}>
      {labels.error[code]}
    </Text>
  </ScrollView>
);


//* STYLES

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },

  error: {
    paddingHorizontal: 20,
    marginTop: 20,
    textAlign: 'center',
  }
});
