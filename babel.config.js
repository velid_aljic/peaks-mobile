module.exports = {
  presets: ['module:metro-react-native-babel-preset', 'module:react-native-dotenv'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          '@assets': './src/assets',
          '@components': './src/components',
          '@data': './src/data',
          '@i18n': './src/i18n',
          '@routes': './src/Routes.js',
          '@screens': './src/screens',
          '@shared': './src/shared',
          '@utils': './src/utils'
        }
      }
    ]
  ]

};
